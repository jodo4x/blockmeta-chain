/**
 * Copyright 2013-2014 Facebook, Inc.
 */

export default function keyMirror(obj) {
  const ret = {}

  if (!(obj instanceof Object && !Array.isArray(obj))) {
    throw new Error('keyMirror(...): Argument must be an object.')
  }

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      ret[key] = key
    }
  }

  return ret
}
