export default function digits(num) {
  return num ? String(num).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") : num
}
