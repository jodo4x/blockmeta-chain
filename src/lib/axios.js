import axios from 'axios'

axios.interceptors.response
.use(function(res) {

  if (process.env.BROWSER) {
    if (res.data.status === 'failure') {
      res.error = {
        'status': 'warning',
        message: '获取信息失败，请再试一次。'
      }

      return res
    }
  }

  return res
}, function(err) {
  return err
})

export default axios
