import createStore from 'redux/lib/createStore'
import applyMiddleware from 'redux/lib/applyMiddleware'
import thunkMiddleware from 'redux-thunk'
import routerMiddleware from 'react-router-redux/lib/middleware'
import browserHistory from 'react-router/lib/browserHistory'

import reducers from '../reducers'

export default function configureStore(initialState = {}) {
  return createStore(
    reducers,
    initialState,
    applyMiddleware(
      thunkMiddleware,
      routerMiddleware(browserHistory) // FIXME: what about `browserHistory` on server?
    )
  )
}
