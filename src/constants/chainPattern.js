// 严格匹配: `bitcoin`, `ethereum` 以及 `ethereumclassic`
const chainPattern = /^(bitcoin|ethereum(classic)?)$/

export default chainPattern
