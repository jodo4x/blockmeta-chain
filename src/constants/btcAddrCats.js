export default {
  bussiness: '商业',
  exchange: '交易所',
  personal: '个人',
  miner: '矿工',
  donation: '捐赠',
  other: '其他',
  hack: '黑客',
  gambling: '博彩',
  all: '全部'
}
