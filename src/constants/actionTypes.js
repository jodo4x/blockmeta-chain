import keyMirror from '../lib/keyMirror'

export default {
  errorMsg: keyMirror({
    showError: null,
    dismissError: null
  }),

  ui: keyMirror({
    toggleDisplayStyle: null,
    toggleTxSwitcher: null
  }),

  homepage: keyMirror({
    loadSearch: null,
    loadBlocks: null
  }),

  stats: keyMirror({
    loadStats: null
  }),

  charts: keyMirror({
    loadCharts: null
  }),

  chart: keyMirror({
    loadChart: null
  }),

  block: keyMirror({
    loadBlock: null
  }),

  tx: keyMirror({
    loadTx: null
  }),

  miner: keyMirror({
    loadMiner: null
  }),

  token: keyMirror({
    loadToken: null
  }),

  address: keyMirror({
    loadAddress: null
  }),

  archives: keyMirror({
    loadArchives: null
  }),

  nodes: keyMirror({
    loadDist: null,
    loadNodes: null,
    searchNodes: null
  })
}
