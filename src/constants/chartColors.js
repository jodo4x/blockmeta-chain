export default [
  '#ef4836',
  '#dcc6e0',
  '#be90d4',
  '#8e44ad',
  '#446cb3',
  '#22a7f0',
  '#2c3e50',
  '#2574a9',
  '#4ecdc4',
  '#f62459',
  '#36d7b7',
  '#2ecc71',
  '#f7ca18',
  '#bf55ec',
  '#f9690e',
  '#dadfe1'
]
