import React from 'react'
import { render } from 'react-dom'
import Provider from 'react-redux/lib/components/Provider'
import match from 'react-router/lib/match'
import Router from 'react-router/lib/Router'
import browserHistory from 'react-router/lib/browserHistory'
import syncHistoryWithStore from 'react-router-redux/lib/sync'

import configureStore from './lib/configureStore'
import routes from './routes'

const store = configureStore(window.__INITIAL_STATE__)
const history = syncHistoryWithStore(browserHistory, store)

match({ history, routes }, (error, redirectLocation, renderProps) => {
  render(
    <Provider store={store}>
      <Router {...renderProps} />
    </Provider>,
    document.getElementById('root')
  )
})
