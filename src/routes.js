// polyfill webpack require.ensure
if (typeof require.ensure !== 'function') {
  require.ensure = function(d, c) {
    c(require)
  }
}

export default [
  {
    path: '/',

    getComponent(nextState, cb) {
      require.ensure([], () => {
        cb(null, require('./components/Layout'))
      })
    },

    indexRoute: {
      getComponent(nextState, cb) {
        require.ensure([], (require) => {
          cb(null, require('./components/Homepage'))
        })
      },
    },

    childRoutes: [
      {
        path: '/stats',
        getComponent(nextState, cb) {
          require.ensure([], () => {
            cb(null, require('./components/Stats'))
          })
        }
      },

      {
        path: '/charts',
        getComponent(nextState, cb) {
          require.ensure([], () => {
            cb(null, require('./components/Charts'))
          })
        }
      },

      {
        path: '/charts/:chart',
        getComponent(nextState, cb) {
          require.ensure([], () => {
            cb(null, require('./components/Chart'))
          })
        }
      },

      {
        path: '/block/:id',
        getComponent(nextState, cb) {
          require.ensure([], () => {
            cb(null, require('./components/Block'))
          })
        }
      },

      {
        path: '/tx/:id',
        getComponent(nextState, cb) {
          require.ensure([], () => {
            cb(null, require('./components/Tx'))
          })
        }
      },

      {
        path: '/address/:id',
        getComponent(nextState, cb) {
          require.ensure([], () => {
            cb(null, require('./components/Address'))
          })
        }
      },

      {
        path: '/miner/:miner',
        getComponent(nextState, cb) {
          require.ensure([], () => {
            cb(null, require('./components/Miner'))
          })
        }
      },

      {
        path: '/token/:token',
        getComponent(nextState, cb) {
          require.ensure([], () => {
            cb(null, require('./components/Token'))
          })
        }
      },

      {
        path: '/archives',
        getComponent(nextState, cb) {
          require.ensure([], () => {
            cb(null, require('./components/Archives'))
          })
        }
      },

      {
        path: '/nodes',
        getComponent(nextState, cb) {
          require.ensure([], () => {
            cb(null, require('./components/Nodes'))
          })
        }
      },

      {
        path: '/api',
        getComponent(nextState, cb) {
          require.ensure([], () => {
            cb(null, require('./components/Api'))
          })
        }
      },

      {
        path: '/about',
        getComponent(nextState, cb) {
          require.ensure([], () => {
            cb(null, require('./components/About'))
          })
        }
      },

      {
        path: '*',
        getComponent(nextState, cb) {
          require.ensure([], () => {
            cb(null, require('./components/NotFound'))
          })
        }
      }
    ]
  },

  {
    path: '/logs',
    getComponent(nextState, cb) {
      require.ensure([], () => {
        cb(null, require('./components/Logs'))
      })
    }
  }
]
