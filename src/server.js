delete process.env.BROWSER

import React from 'react'
import { renderToString } from 'react-dom/server'
import Provider from 'react-redux/lib/components/Provider'
import match from 'react-router/lib/match'
import RouterContext from 'react-router/lib/RouterContext'

import configureStore from './lib/configureStore'
import routes from './routes'

const store = configureStore()

function renderFullPage(markup, initialState) {
  return `
  <!doctype html>
  <html>
    <head>
      <meta charset="utf-8" />
      <title>blockmeta</title>
      <script>window.__INITIAL_STATE__ = ${JSON.stringify(initialState)}</script>
      <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
      <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
      <!-- Flag sprites service provided by Martijn Lafeber, https://github.com/lafeber/world-flags-sprite/blob/master/LICENSE -->
      <link href="//cloud.github.com/downloads/lafeber/world-flags-sprite/flags32.css" rel="stylesheet" />
      <link href="/main.css" rel="stylesheet" />
    </head>
    <body>
      <div id="root">${markup}</div>
      <script src="/main.js"></script>
    </body>
  </html>
  `
}

import fs from 'fs'
import zlib from 'zlib'
import express from 'express'

const app = express()

app.use(express.static(`${__dirname}/../build`))

app.use((req, res) => {

  // Server JavaScript assets
  if (/build/.test(req.url)) {
    return fs.readFile(`.${req.url}`, (err, data) => {
      zlib.gzip(data, (err, result) => {
        res.header({
          'Content-Length': result.length,
          'Content-Type': 'text/javascript',
          'Content-Encoding': 'gzip',
        })
        res.status(200).send(result)
      })
    })
  }

  match({ routes, 'location': req.url }, (error, redirectLocation, renderProps) => {
    // TODO: error, redirectLocation
    
    if (renderProps) {
      const { location, params } = renderProps

      Promise.all(
        renderProps.components
        .filter(c => c && c.fetchData)
        .map(c => c.fetchData({ store, location, params }))
      )
      .then(() => {
        const initialState = store.getState()

        const markup = renderToString(
          <Provider store={store}>
            <RouterContext {...renderProps} />
          </Provider>
        )

        res.status(200).send(renderFullPage(markup, initialState))
      })
    }
  })
})

const PORT = 2345

app.listen(PORT, () => {
  console.log(`App serving at http://127.0.0.1:${PORT}`)
})
