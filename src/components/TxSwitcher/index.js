import React, { PropTypes, Component } from 'react'
import bindActionCreators from 'redux/lib/bindActionCreators'
import connect from 'react-redux/lib/components/connect'
import cookie from 'cookie'
import { toggleTxSwitcher } from '../../actions/ui'

if (process.env.BROWSER) {
  require('./index.scss')
}

class TxSwitcher extends Component {
  constructor(props) {
    super()
    this.handleClick = this.handleClick.bind(this)
  }

  componentDidMount() {
    const { txSwitchType } = cookie.parse(document.cookie)

    // 如果cookie中`txSwitchType`不等于`v`, 都让它保持默认的`h`形式
    if (txSwitchType && txSwitchType === 'v') {
      this.props.toggleTxSwitcher(txSwitchType) // NOTE: 因为外部需要访问该状态，所以必须同时更新整个state中的值
    }
  }

  render() {
    const {
      activeType
    } = this.props

    return (
      <div className="tx-switcher">
        <div className="btn-group btn-group-xs">
          <button
            className={`btn btn-default ${activeType === 'h' ? 'active' : ''}`}
            type="button"
            onClick={() => this.handleClick('h')}
          >
            <span>水平</span>
          </button>
          <button
            className={`btn btn-default ${activeType === 'v' ? 'active' : ''}`}
            type="button"
            onClick={() => this.handleClick('v')}
          >
            <span>垂直</span>
          </button>
        </div>
      </div>
    )
  }

  handleClick(value) {
    this.props.toggleTxSwitcher(value)
    document.cookie = cookie.serialize(
      'txSwitchType',
      value,
      {
        path: '/',
        maxAge: 60*60*24*30
      }
    )
  }
}

TxSwitcher.propTypes = {
  toggleTxSwitcher: PropTypes.func.isRequired,
  activeType: PropTypes.string.isRequired
}

export default connect(
  state => ({
    activeType: state.ui.txSwitchType
  }),

  dispatch => bindActionCreators({ toggleTxSwitcher }, dispatch)
)(TxSwitcher)
