import React, { PropTypes, Component } from 'react'
import bindActionCreators from 'redux/lib/bindActionCreators'
import connect from 'react-redux/lib/components/connect'
import loadStats from '../../actions/stats'
import Highcharts from 'highcharts'
import h3d from 'highcharts/highcharts-3d'
import digits from '../../lib/digits'
import Link from 'react-router/lib/Link'
import chartColors from '../../constants/chartColors'

if (process.env.BROWSER) {
  require('./index.scss')
}

class Stats extends Component {
  static fetchData({ store }) {
    return store.dispatch(loadStats())
  }

  constructor() {
    super()

    this.state = {
      selected: 'day',
      tabs: {
        day: '今天',
        week: '本周',
        month: '本月'
      }
    }

    ;[
      'handleSelect',
      'renderDist'
    ].forEach((handler) => {
      this[handler] = this[handler].bind(this)
    })
  }

  componentDidMount() {
    this.props.loadStats()
    .then(({ type, payload }) => {
      const {
        pool_dist: {
          day,
          week,
          month
        }
      } = this.props.stats

      h3d(Highcharts)

      this.chart = new Highcharts.Chart(this.refs.chart, {
        title: { text: null },
        tooltip: { shadow: false },
        colors: chartColors,
        chart: {
          type: 'pie',
          options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
          }
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 30, // enable 3d
          }
        },
        series: [{
          name: "数量",
          type: "pie",
          data: day
        }],
        credits: { enabled: false }
      })
    })
  }

  componentWillUnmount() {
    this.chart &&
    this.chart.destroy()
  }

  render() {
    const {
      stats: {
        blockchain,
        pool_dist,
        market
      }
    } = this.props

    return (
      <div className="container">
        <div className="stats">
          <div>
            <h2>统计<small className="text-muted">(比特币)</small></h2>
            <div className="row">
              <div className="col-sm-6">
                <table className="table table-striped">
                  <caption>区块数据</caption>
                  <colgroup>
                    <col className="th" />
                    <col className="td" />
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>大小</th>
                      <td>{blockchain.blockchain_size} <em className="text-muted">GB</em></td>
                    </tr>
                    <tr>
                      <th>块数量</th>
                      <td>{digits(blockchain.blocks_num)}</td>
                    </tr>
                    <tr>
                      <th>当前难度</th>
                      <td>{digits(blockchain.difficulty)}</td>
                    </tr>
                    <tr>
                      <th>未来难度<sup className="text-muted">预计</sup></th>
                      <td>{digits(blockchain.est_difficulty)}</td>
                    </tr>
                    <tr>
                      <th>距离难度调整时间</th>
                      <td>{blockchain.est_time}{/* TODO: miniutes */}</td>
                    </tr>
                    <tr>
                      <th>平均块间隔时间</th>
                      <td>{blockchain.avg_block_time} <em className="text-muted">分钟</em></td>
                    </tr>
                    <tr>
                      <th>当前算力</th>
                      <td>{blockchain.hashrate} <em className="text-muted">PH/s</em></td>
                    </tr>
                    <tr>
                      <th>奖励</th>
                      <td>{blockchain.reward} <em className="text-muted">BTC</em></td>
                    </tr>
                    <tr>
                      <th>链上地址总数</th>
                      <td>{digits(blockchain.total_addrs)}</td>
                    </tr>
                    <tr>
                      <th>链上交易总数</th>
                      <td>{digits(blockchain.total_txs)}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-sm-6">
                <table className="table table-striped">
                  <caption>市场价值</caption>
                  <colgroup>
                    <col className="th" />
                    <col className="td" />
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>当前价格</th>
                      <td>{digits(market.price)} <em className="text-muted">CNY</em></td>
                    </tr>
                    <tr>
                      <th>已产出总币量</th>
                      <td>{digits(market.total)}</td>
                    </tr>
                    <tr>
                      <th>总市值</th>
                      <td>{digits(market.capacity)} <em className="text-muted">CNY</em></td>
                    </tr>
                    <tr>
                      <th>交易量<sup className="text-muted">24H</sup></th>
                      <td>{digits(market.trade)}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          {this.renderDist(pool_dist)}

        </div>
      </div>
    )
  }

  handleSelect(eventKey) {
  }

  renderDist(pool_dist) {
    const { selected, tabs } = this.state

    return (
      <div>
        <h2>矿池分布</h2>
        <div className="row">
          <div className="col-sm-8">
            <div style={{ width: '100%', height: 300 }} ref="chart">正在加载...</div>
            <ul className="nav nav-pills">
              {Object.keys(tabs).map((key, index) => (
                <li className={selected === key ? 'active' : ''} key={index}>
                  <a href onClick={(e) => this.handleSelect(e, key)}>{tabs[key]}</a>
                </li>
              ))}
            </ul>
          </div>
          <div className="col-sm-4">
            <ul className="pool-list clearfix">
              {pool_dist.day && pool_dist.day.map((dist, index) => (
                <li key={index}>
                  <i className={`icon-pool icon-pool-${dist[0] ? dist[0].replace(/\./g, '').toLowerCase() : 'unknown'}`} />
                  {dist[0] !== 'Unknown' ? (
                    <strong><Link to={`/miner/${dist[0]}`}>{dist[0]}</Link><sup className="badge">{dist[1]}</sup></strong>
                  ) : (
                    <span>{dist[0]}<sup className="badge">{dist[1]}</sup></span>
                  )}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

Stats.propTypes = {
  loadStats: PropTypes.func.isRequired,
  stats: PropTypes.shape({
    pool_dist: PropTypes.object.isRequired,
    blockchain: PropTypes.object.isRequired,
    market: PropTypes.object.isRequired
  }).isRequired
}

export default connect(
  state => ({
    stats: state.stats
  }),
  dispatch => bindActionCreators({
    loadStats
  }, dispatch)
)(Stats)
