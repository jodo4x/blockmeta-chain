import React from 'react'
import Link from 'react-router/lib/Link'
import moment from 'moment'

export default function Logs() {
  const updatedAt = new Date('2016-10-10 12:39:20')

  return (
    <div style={{ width: 650, margin: '50px auto' }}>
      <div className="well">
        <div className="page-header">
          <h1 className="text-center">
            <Link to="/"><span title="Back to home"><strong>chain</strong>.reviews</span></Link>
          </h1>
          <div>
            <strong>Latest update</strong>:
            <time>{moment(updatedAt.getTime()).fromNow()}</time>
          </div>
        </div>

        <div className="alert alert-info">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </div>

        <ol className="">
          <li>
            <h2>Fix browser bundle for AMD (#8374)</h2>
            <div><a href="https://github.com/helloruli">helloruli</a> committed with <a href="https://github.com/spicyj">spicyj</a> <span className="text-muted">11 hours ago</span></div>
          </li>
          <li>
            <h2>Passing disabled events</h2>
            <div><a href>sebmarkbage</a> committed <span className="text-muted">12 hours ago</span></div>
          </li>
          <li>
            <h2>Disable one irrelevant test for Fiber</h2>
            <div><a href>sebmarkbage</a> committed <span className="text-muted">3 days ago</span></div>
          </li>
        </ol>

        <hr />

        <div className="text-center text-muted">
          <span>&copy; 2016 chain.reviews</span>
        </div>
      </div>
    </div>
  )
}
