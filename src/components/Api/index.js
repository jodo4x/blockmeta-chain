import React, { Component } from 'react'
import { Events, Link, Element } from 'react-scroll'

if (process.env.BROWSER) {
  require('./index.scss')
}

export default class API extends Component {
  constructor() {
    super()
    this.handleScroll = this.handleScroll.bind(this)
  }

  handleScroll() {
    this.menu.setAttribute(
      'style',
      window.scrollY >= this.state.initialTop ?
      `position: fixed; top: 0; width: ${this.state.initialWidth}px;` :
      ''
    )
  }

  componentDidMount() {
    Events.scrollEvent.register('begin')
    Events.scrollEvent.register('end')

    // NOTE: 只能在浏览器环境下工作
    const offset = require('document-offset')

    this.setState({
      initialWidth: this.menu.getBoundingClientRect().width,
      initialTop: offset(this.menu).top
    }, () => {
      window.addEventListener('scroll', this.handleScroll, false)
    })
  }

  componentWillUnmount() {
    Events.scrollEvent.remove('begin')
    Events.scrollEvent.remove('end')
    window.removeEventListener('scroll', this.handleScroll, false)
  }

  render() {
    return (
      <div className="container">
        <div className="api">
          <div className="row">
            <div className="col-sm-3">
              <div ref={c => this.menu = c}>
                <div className="list-group text-capitalize">
                  <Link
                    className="list-group-item"
                    href
                    activeClass="active"
                    spy={true}
                    smooth={true}
                    to="common"
                  >
                    通用
                  </Link>
                  <Link
                    className="list-group-item"
                    href
                    activeClass="active"
                    spy={true}
                    smooth={true}
                    to="blockchain"
                  >
                    区块
                  </Link>
                  <Link
                    className="list-group-item"
                    href
                    activeClass="active"
                    spy={true}
                    smooth={true}
                    to="transcation"
                  >
                    交易
                  </Link>
                  <Link
                    className="list-group-item"
                    href
                    activeClass="active"
                    spy={true}
                    smooth={true}
                    to="address"
                  >
                    地址
                  </Link>
                  <Link
                    className="list-group-item"
                    href
                    activeClass="active"
                    spy={true}
                    smooth={true}
                    to="tools"
                  >
                    工具
                  </Link>
                  <Link
                    className="list-group-item"
                    href
                    activeClass="active"
                    spy={true}
                    smooth={true}
                    to="websocket"
                  >
                    Web Socket
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-sm-9">
              <div>
                <div>
                  <h1>开发者API文档</h1>
                  <div>
                    <p>所有API请求支持HTTP和HTTPS，<span className="text-warning">除非特别说明</span>。</p>
                    <p>请求返回数据为<u>JSON</u>格式，遵循的标准为:</p>
                    <pre>{`{
    "status": "success",
    "data": { "foo": "bar" },
    "message": "Error Message"
  }`}</pre>
                  </div>
                  <Element name="common" className="element clearfix">
                    <h2>常用</h2>

                    <h3>难度</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/getdifficulty`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/chain/getdifficulty" target="_blank">
                            {`https://blockmeta.com/api/v1/chain/getdifficulty`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/getdifficulty`}</pre>
                      </dd>
                    </dl>

                    <h3>主链上的最大高度</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/getblockcount`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/getblockcount" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/getblockcount`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/getblockcount`}</pre>
                      </dd>
                    </dl>

                    <h3>流通的比特币总数</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/totalbtc`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/totalbtc" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/totalbtc`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/totalbtc`}</pre>
                      </dd>
                    </dl>

                    <h3>最新块的Hash值</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/lastblockhash`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/lastblockhash" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/lastblockhash`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd><pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/lastblockhash`}</pre></dd>
                    </dl>

                    <h3>最新块的开采者</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/lastminer`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/lastminer" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/lastminer`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/lastminer`}</pre>
                      </dd>
                    </dl>

                    <h3>当前挖矿奖励</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/reward`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/reward" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/reward`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/reward`}</pre>
                      </dd>
                    </dl>

                    <h3>块平均交易数(默认是100个块)</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/avgtxnum`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/avgtxnum" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/avgtxnum`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/avgtxnum`}</pre>
                      </dd>
                    </dl>

                    <h3>块平均间隔时间(默认是2016个块)</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/interval`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/interval" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/interval`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/interval`}</pre>
                      </dd>
                    </dl>

                    <h3>下次变更难度(估计值)</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/nextdifficulty`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/nextdifficulty" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/nextdifficulty`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/nextdifficulty`}</pre>
                      </dd>
                    </dl>

                    <h3>全网未确认交易数</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/unconfirmedcount`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/unconfirmedcount" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/unconfirmedcount`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/unconfirmedcount`}</pre>
                      </dd>
                    </dl>

                    <h3>全网算力(估计值。单位: <code>hash/s</code>)</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/hashrate`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/hashrate" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/hashrate`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/hashrate`}</pre>
                      </dd>
                    </dl>

                    <h3>区块链总大小(单位:<code>byte</code>)</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/chainsize`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/chainsize" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/chainsize`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/chainsize`}</pre>
                      </dd>
                    </dl>

                    <h3>24小时内矿池挖矿的比例</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/24hrpoolstat`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/24hrpoolstat" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/24hrpoolstat`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/24hrpoolstat`}</pre>
                      </dd>
                    </dl>

                    <h3>24小时内产块数</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/24hrblockcount`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/24hrblockcount" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/24hrblockcount`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/24hrblockcount`}</pre>
                      </dd>
                    </dl>

                    <h3>实时行情</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/chain/ticker`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/chain/ticker" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/chain/ticker`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/chain/ticker`}</pre>
                      </dd>
                    </dl>

                  </Element>

                  <Element name="blockchain" className="element clearfix">
                    <h2>区块数据</h2>

                    <h3>区块统计数据</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/block/<block_height|block_hash>?q=info`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/block/0000000000000000012ea09ad27b0bbdfa004c110cc8c4ea914d75b28a5c8df0?q=info" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/block/0000000000000000012ea09ad27b0bbdfa004c110cc8c4ea914d75b28a5c8df0?q=info`}
                            <i className="fa fa-external-link" />
                          </a>
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/block/123456?q=info
curl -X GET https://blockmeta.com/api/v1/btc/block/0000000000000000012ea09ad27b0bbdfa004c110cc8c4ea914d75b28a5c8df0?q=info`}</pre>
                      </dd>
                    </dl>
                    <p>block_height 也可以支持 <code>first</code>创世块, <code>last</code>最新块，比如:</p>
                    <div className="indent">
                      <dl className="dl-horizontal">
                        <dt>GET</dt>
                        <dd>
                          <code>{`/api/v1/btc/block/<first|last>?q=info`}</code>
                        </dd>
                        <dt>URL</dt>
                        <dd>
                          <code>
                            <a href="https://blockmeta.com/api/v1/btc/block/first?q=info" target="_blank">
                              {`https://blockmeta.com/api/v1/btc/block/first?q=info`}
                            </a>
                            <i className="fa fa-external-link" />
                          </code>
                        </dd>
                        <dt>CURL</dt>
                        <dd>
                          <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/block/last?q=info`}</pre>
                        </dd>
                      </dl>
                    </div>

                    <h3>区块交易数据</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/block/<block_height|block_hash>?q=tx`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/block/123455?q=tx" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/block/123455?q=tx`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/block/123455?q=tx`}</pre>
                      </dd>
                    </dl>
                    <p>block_height 也可以支持 <code>first</code>创世块, <code>last</code>最新块，比如:</p>
                    <div className="indent">
                      <dl className="dl-horizontal">
                        <dt>GET</dt>
                        <dd>
                          <code>{`/api/v1/btc/block/<first|last>?q=tx`}</code>
                        </dd>
                        <dt>URL</dt>
                        <dd>
                          <code>
                            <a href="https://blockmeta.com/api/v1/btc/block/last?q=tx" target="_blank">
                              {`https://blockmeta.com/api/v1/btc/block/last?q=tx`}
                            </a>
                            <i className="fa fa-external-link" />
                          </code>
                        </dd>
                        <dt>CURL</dt>
                        <dd>
                          <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/block/last?q=tx`}</pre>
                        </dd>
                      </dl>
                    </div>

                  </Element>

                  <Element name="transcation" className="element clearfix">
                    <h2>交易</h2>

                    <h3>交易统计数据</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/tx/<tx_hash>?q=info`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/tx/6b2b797ad47ca3f39593534cc6d03a46a6a7b5f47d9434dc1d119b97643c7682?q=info" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/tx/6b2b797ad47ca3f39593534cc6d03a46a6a7b5f47d9434dc1d119b97643c7682?q=info`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/tx/6b2b797ad47ca3f39593534cc6d03a46a6a7b5f47d9434dc1d119b97643c7682?q=info`}</pre>
                      </dd>
                    </dl>

                  </Element>

                  <Element name="address" className="element clearfix">
                    <h2>地址</h2>

                    <h3>地址统计数据</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/address/<address|hash160>?q=info`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/address/1Q3FsFTs6Qz3LJ3rqCStbLJmomBqqKRx1Z?q=info" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/address/1Q3FsFTs6Qz3LJ3rqCStbLJmomBqqKRx1Z?q=info`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/address/1Q3FsFTs6Qz3LJ3rqCStbLJmomBqqKRx1Z?q=info
curl -X GET https://blockmeta.com/api/v1/btc/address/fcb822804656cafd49daff890eaccfe95f59cfaf?q=info
                        `}</pre>
                      </dd>
                    </dl>

                    <h3>地址未确认交易数据</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/address/<address|hash160>?q=unconfirmed`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/address/1Q3FsFTs6Qz3LJ3rqCStbLJmomBqqKRx1Z?q=unconfirmed" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/address/1Q3FsFTs6Qz3LJ3rqCStbLJmomBqqKRx1Z?q=unconfirmed`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/address/1Q3FsFTs6Qz3LJ3rqCStbLJmomBqqKRx1Z?q=unconfirmed
curl -X GET https://blockmeta.com/api/v1/btc/address/fcb822804656cafd49daff890eaccfe95f59cfaf?q=unconfirmed
                        `}</pre>
                      </dd>
                    </dl>

                    <h3>地址未花费输入(比特币)</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/address/<address|hash160>?q=unspent`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/address/1Q3FsFTs6Qz3LJ3rqCStbLJmomBqqKRx1Z?q=unspent" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/address/1Q3FsFTs6Qz3LJ3rqCStbLJmomBqqKRx1Z?q=unspent`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/address/1Q3FsFTs6Qz3LJ3rqCStbLJmomBqqKRx1Z?q=unspent`}</pre>
                      </dd>
                    </dl>

                  </Element>

                  <Element name="tools" className="element clearfix">
                    <h2>区块链工具</h2>

                    <h3><code>hash160</code>与地址进行转换</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/tool/hashtoaddress?q=<hash160>`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/tool/hashtoaddress?q=fcb822804656cafd49daff890eaccfe95f59cfaf" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/tool/hashtoaddress?q=fcb822804656cafd49daff890eaccfe95f59cfaf`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/tool/hashtoaddress?q=fcb822804656cafd49daff890eaccfe95f59cfaf`}</pre>
                      </dd>
                    </dl>

                    <h3>地址与<code>hash160</code>进行转换</h3>
                    <dl className="dl-horizontal">
                      <dt>GET</dt>
                      <dd>
                        <code>{`/api/v1/btc/tool/addresstohash?q=<address>`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>
                          <a href="https://blockmeta.com/api/v1/btc/tool/addresstohash?q=fcb822804656cafd49daff890eaccfe95f59cfaf" target="_blank">
                            {`https://blockmeta.com/api/v1/btc/tool/addresstohash?q=fcb822804656cafd49daff890eaccfe95f59cfaf`}
                          </a>
                          <i className="fa fa-external-link" />
                        </code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -X GET https://blockmeta.com/api/v1/btc/tool/addresstohash?q=fcb822804656cafd49daff890eaccfe95f59cfaf`}</pre>
                      </dd>
                    </dl>

                    <h3>交易推送</h3>
                    <dl className="dl-horizontal">
                      <dt>POST</dt>
                      <dd>
                        <code>{`/api/v1/btc/tool/pushtx`}</code>
                      </dd>
                      <dt>URL</dt>
                      <dd>
                        <code>{`https://blockmeta.com/api/v1/btc/tool/pushtx`}</code>
                      </dd>
                      <dt>CURL</dt>
                      <dd>
                        <pre>{`curl -d '{"raw_tx": [RAW_TX,]}' http://blockmeta.com/api/v1/tool/pushtx --header "Content-Type:application/json"`}</pre>
                      </dd>
                    </dl>
                  </Element>

                  <Element name="websocket" className="element clearfix">
                    <h2>WebSocket</h2>
                    <p>高效实时低延迟的数据查询接口，可支持最新区块，最新交易的查询。</p>

                    <pre>{`wss://blockmeta.com/ws`}</pre>

                    <h3>支持命令</h3>
                    <ul className="websocket-list">
                      <li>
                        <h4>订阅接收比特币网络全部未确认交易</h4>
                        <pre>{`{"op":"unconfirmed_tx_sub","data":[]}`}</pre>
                        <h4>获得响应格式</h4>
                        <pre>{`{"op": "unconfirmed_tx_sub", "data":[<tx>]}`}</pre>
                        <h4>tx 结构如下</h4>
                        <pre>{`{
  "height": 高度,
  "relay":  矿工,
  "time":   时间,
  "tx_num": 交易时间,
  "fee":    手续费,
  "id":     id
}`}</pre>
                      </li>
                      <li>
                        <h4>订阅接收最新区块通知信息</h4>
                        <pre>{`{ "op":"blocks_sub", "data":[] }`}</pre>
                        <h4>获得响应格式</h4>
                        <pre>{`{ "op":"blocks_sub", "data":[{<block>}] }`}</pre>
                        <h4>block 结构如下</h4>
                        <pre>{`{
  "height": 高度,
  "relay":  矿工,
  "time":   时间,
  "tx_num": 交易时间,
  "fee":    手续费,
  "id":     id
}`}</pre>
                      </li>
                      <li>
                        <h4>订阅具体地址的新交易信息</h4>
                        <pre>{`{ "op": "addr_sub", "data":[<addr1>, <addr2>, ...] }`}</pre>
                        <h4>获得响应格式</h4>
                        <pre>{`{ "op": "addr_sub", "data":[{"addr":<addr1>, "txs":[...]},...] }`}</pre>
                      </li>
                    </ul>

                    <h3>取消订阅命令</h3>
                    <pre>{`{
  "op": "clear_sub",
  "data":[<cmd1>, <cmd2>, ...]
}`}</pre>
                      <p>定制命令包括之前三个命令，同时支持 <code>"all"</code> 取消订阅所有命令。</p>
                  </Element>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
