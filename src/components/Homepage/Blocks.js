import React, { PropTypes, Component } from 'react'
import Link from 'react-router/lib/Link'
import Pagination from 'react-bootstrap/lib/Pagination'
import digits from '../../lib/digits'

import BlockList from './BlockList'

export default class Blocks extends Component {
  render() {
    const {
      data: {
        activeKey,
        chainMap,
        blocks: {
          items,
          no_page,
          pages
        }
      },
      handleSelect,
      handlePagination
    } = this.props

    return (
      <div className="recent-blocks-container">
        <h2>最新块</h2>
        <ul className="nav nav-tabs">
          <li className={activeKey === 1 ? 'active': ''}>
            <a href onClick={(e) => handleSelect(e, 1)}>比特币</a>
          </li>
          <li className={activeKey === 2 ? 'active': ''}>
            <a href onClick={(e) => handleSelect(e, 2)}>以太坊分叉</a>
          </li>
          <li className={activeKey === 3 ? 'active': ''}>
            <a href onClick={(e) => handleSelect(e, 3)}>以太坊原链</a>
          </li>
        </ul>

        <div>
          <BlockList
            data={{ activeKey, chainMap, items }}
          />

          <div className="pager-container">
            <Pagination
              prev
              next
              maxButtons={10}
              activePage={no_page}
              items={pages}
              onSelect={handlePagination}
            />
          </div>
        </div>
      </div>
    )
  }
}

Blocks.propTypes = {
  handleSelect: PropTypes.func.isRequired,
  handlePagination: PropTypes.func.isRequired,
  data: PropTypes.shape({
    activeKey: PropTypes.number.isRequired,
    chainMap: PropTypes.object.isRequired,
    blocks: PropTypes.shape({
      items: PropTypes.array.isRequired,
      no_page: PropTypes.number.isRequired,
      pages: PropTypes.number.isRequired
    }).isRequired
  }).isRequired
}
