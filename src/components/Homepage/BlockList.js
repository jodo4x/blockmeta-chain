import React, { PropTypes } from 'react'
import Link from 'react-router/lib/Link'
import digits from '../../lib/digits'
import moment from 'moment'
import Price from '../Price'

import 'moment/locale/zh-cn'
moment.locale('zh')

export default function BlockList({
  data: {
    activeKey,
    chainMap,
    items
  }
}) {
  return activeKey === 1 ? (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>高度</th>
          <th>时间</th>
          <th>矿池</th>
          <th>交易数</th>
          <th>收入<span className="text-muted">(BTC)</span></th>
          <th>区块大小<span className="text-muted">(KB)</span></th>
        </tr>
      </thead>
      <tbody>
        {items.map((block, index) => (
          <tr key={index}>
            <td><Link to={`/block/${block.height}?chain=${chainMap[activeKey]}`}>{digits(block.height)}</Link></td>
            <td>{moment(block.time*1000).fromNow()}</td>
            <td>
              <i className={`icon-pool icon-pool-${block.relay ? block.relay.replace(/\./g, '').toLowerCase() : 'unknown'}`} />
              {block.relay !== 'Unknown' ? (
                <Link to={`/miner/${block.relay}?chain=${chainMap[activeKey]}`}>{block.relay}</Link>
              ) : (
                <span>未知</span>
              )}
            </td>
            <td>{digits(block.tx_num)}</td>
            <td>
              <Price value={block.fee} />
            </td>
            <td>{(block.size/1024).toFixed(2)}</td>
          </tr>
        ))}
      </tbody>
    </table>
  ) : (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>高度</th>
          <th>时间</th>
          <th>矿工</th>
          <th>交易数</th>
          <th>奖励<span className="text-muted">({activeKey === 2 ? 'ETH' : 'ETC'})</span></th>
          <th>难度</th>
          <th>叔区块数量</th>
          <th>使用瓦斯</th>
        </tr>
      </thead>
      <tbody>
        {items.map((block, index) => (
          <tr key={index}>
            <td><Link to={`/block/${block.number}?chain=${chainMap[activeKey]}`}>{digits(block.number)}</Link></td>
            <td>{moment(block.timestamp*1000).fromNow()}</td>
            <td><span>{block.miner}</span></td>
            <td>{digits(block.txs_num)}</td>
            <td><Price value={block.reward} /></td>
            <td>{digits(block.difficulty)}</td>
            <td>{digits(block.num_uncle)}</td>
            <td>{digits(block.gasUsed)}</td>
          </tr>
        ))}
      </tbody>
    </table>
  )
}

BlockList.propTypes = {
  data: PropTypes.shape({
    activeKey: PropTypes.number.isRequired,
    chainMap: PropTypes.object.isRequired,
    items: PropTypes.array.isRequired
  }).isRequired
}
