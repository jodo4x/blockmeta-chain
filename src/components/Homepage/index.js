import React, { PropTypes, Component } from 'react'
import bindActionCreators from 'redux/lib/bindActionCreators'
import connect from 'react-redux/lib/components/connect'
import { loadSearch, loadBlocks } from '../../actions/homepage'

import SearchBar from './SearchBar'
import Blocks from './Blocks'

if (process.env.BROWSER) {
  require('./index.scss')
}

class Homepage extends Component {
  static fetchData({ store }) {
    return store.dispatch(loadBlocks(/* 'bitcoin' */))
  }

  constructor() {
    super()

    this.state = {
      activeKey: 1,
      chainMap: {
        '1': 'bitcoin',
        '2': 'ethereum',
        '3': 'ethereumclassic'
      }
    }

    ;[
      'handleSearch',
      'handleSelect',
      'handlePagination'
    ].forEach((handler) => {
      this[handler] = this[handler].bind(this)
    })
  }

  componentDidMount() {
    this.props.loadBlocks()
  }

  render() {
    const { activeKey, chainMap } = this.state
    const { blocks } = this.props

    return (
      <div className="homepage">

        <div className="hero">
          <div className="page-header text-center">
            <h1>区块链浏览器</h1>
          </div>

          <SearchBar
            handleSearch={this.handleSearch}
          />
        </div>

        <div className="container">
          <Blocks
            data={{ activeKey, chainMap, blocks }}
            handleSelect={this.handleSelect}
            handlePagination={this.handlePagination}
          />
        </div>
      </div>
    )
  }

  handleSearch(e, activeKey/* NOTE: 这里的`activeKey`是`SearchBar`子组建中的`activeKey` */, value) {
    e.preventDefault()
    this.props.loadSearch(this.state.chainMap[activeKey], value)
  }

  handleSelect(e, activeKey) {
    e.preventDefault()

    this.props.loadBlocks(this.state.chainMap[activeKey])
    .then(() => {
      this.setState({
        activeKey: activeKey
      })
    })
  }

  handlePagination(page) {
    const { activeKey, chainMap } = this.state
    this.props.loadBlocks(
      chainMap[activeKey],
      page
    )
  }
}

Homepage.propTypes = {
  loadSearch: PropTypes.func.isRequired,
  loadBlocks: PropTypes.func.isRequired,
  blocks: PropTypes.shape({
    items: PropTypes.array.isRequired,
    no_page: PropTypes.number.isRequired,
    pages: PropTypes.number.isRequired
  }).isRequired
}

export default connect(
  state => ({
    blocks: state.homepage
  }),

  dispatch => bindActionCreators({
    loadBlocks,
    loadSearch
  }, dispatch)
)(Homepage)
