import React, { PropTypes, Component } from 'react'
import Link from 'react-router/lib/Link'
import Dropdown from 'react-bootstrap/lib/Dropdown'
import MenuItem from 'react-bootstrap/lib/MenuItem'

export default class SearchBar extends Component {
  constructor() {
    super()
    this.state = {
      activeKey: 1,
      options: {
        '1': 'Bitcoin',
        '2': 'Ethereum',
        '3': 'Ethereum classic'
      },
      icons: {
        '1': 'btc',
        '2': 'eth',
        '3': 'etc'
      }
    }

    this.handleSearchDropdown = this.handleSearchDropdown.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
  }

  render() {
    const { activeKey, options, icons } = this.state

    return (
      <div className="searchbar">
        <div className="container">
          <form onSubmit={this.handleSearch}>
            <div className="row">
              <div className="col-sm-9">
                <div className="form-control-container">
                  <div className="addon">
                    <Dropdown
                      onSelect={this.handleSearchDropdown}
                      id="id__dropdown"
                    >
                      <Dropdown.Toggle bsSize="large"><span className={`curr-icon icon-${icons[activeKey]}`} /></Dropdown.Toggle>
                      <Dropdown.Menu>
                        <MenuItem eventKey={1}>Bitcoin</MenuItem>
                        <MenuItem eventKey={2}>Ethereum</MenuItem>
                        <MenuItem eventKey={3}>Ethereum classic</MenuItem>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                  <input
                    className="form-control input-lg"
                    type="text"
                    placeholder="Address/Height/Hash..."
                    ref="keyword"
                  />
                </div>
              </div>
              <div className="col-sm-3 btn-container">
                <button className="btn btn-success btn-lg btn-block" type="submit">
                  <span>搜索</span>
                </button>
              </div>
            </div>

            <p className="help-block">
              <strong>趋势:</strong>
              <Link to="/address/15h6A2a3D31vRviBDdSpvhLtYJq3aePhdW?chain=bitcoin">Asicminer</Link>
              <Link to="/address/18e5jpfn9zo9GQov1Ks9Pzh6Yam2SvFqqo?chain=bitcoin">SatoshiDice</Link>
              <Link to="/address/36PrZ1KHYMpqSyAQXSG8VwbUiq2EogxLo2?chain=bitcoin">Ethereum IPO</Link>
              <Link to="/token/DCS?chain=ethereum">DCS</Link>
              <Link to="/token/DGD?chain=ethereum">DGD</Link>
            </p>
          </form>
        </div>
      </div>
    )
  }

  handleSearchDropdown(eventKey) {
    this.setState({
      activeKey: eventKey
    })
  }

  handleSearch(e) {
    e.preventDefault()

    // NOTE:
    // 由于`SearchBar`和整个`Homepage`共享一个`activeKey`的话,
    // 在`SearchBar`选择链的时候,会影响到`Blocks`的Tab.
    // 所以在这里,单独设置一个`activeKey`, 然后把它向外传出去以适配搜索的`chain`参数.
    this.props.handleSearch(e, this.state.activeKey, this.refs.keyword.value) // 这里传出去`chain`的值
  }
}

SearchBar.propTypes = {
  handleSearch: PropTypes.func.isRequired
}
