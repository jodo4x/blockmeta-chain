import React, { PropTypes } from 'react'
import digits from '../../lib/digits'

export default function Price({ value }) {
  let ret = null

  if (value) {
    const matched = `${value}`.match(/((?:\+|\-)?\d*\.\d{4})(\d+)/)

    if (matched) {
      ret = (
        <span className="span-price">
          {matched.slice(1).map((value, index) => (
            <span className={RegExp.$2 === value ? 'text-muted' : 'text-solid'} key={index}>
              {value}
            </span>
          ))}
        </span>
      )
    } else {
      ret = <span className={`span-price ${/\-/.test(value) ? 'text-danger' : ''}`}>{value}</span>
    }
  }

  return ret
}
