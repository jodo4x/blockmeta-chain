import React, { PropTypes, Component } from 'react'
import bindActionCreators from 'redux/lib/bindActionCreators'
import connect from 'react-redux/lib/components/connect'
import Link from 'react-router/lib/Link'
import Highcharts from 'highcharts'
import loadCharts from '../../actions/charts'

class Charts extends Component {
  static fetchData({ store }) {
    return store.dispatch(loadCharts(/* NOTE: 默认的是`bitcoin` */))
  }

  constructor() {
    super()
    this.state = {
      chartMap: {
        '0': 'total_fees',
        '1': 'block_avg_size',
        '2': 'block_avg_interval',
        '3': 'totalbtc',
        '4': 'blockchain_size',
        '5': 'hash_rate',
      },
      chartInstances: []
    }
    this.renderChart = this.renderChart.bind(this)
  }

  componentDidMount() {
    const { location, loadCharts } = this.props

    loadCharts(location.query.chain)
    .then(({ type, payload }) => {
      const { refs, state: { chartMap } } = this
      payload.charts.forEach((chartData, index) => {
        this.renderChart(
          refs[chartMap[index]],
          chartData.map(item => ([
            item[0]*1000,
            parseFloat(item[1])
          ]))
        )
      })
    })
  }

  componentWillUnmount() {
    let instance
    while(
      instance = this.state.chartInstances.shift()
    ) {
      instance.destroy()
    }
  }

  render() {
    const { chartMap } = this.state
    const { charts } = this.props

    return (
      <div className="container">
        <ul className="breadcrumb">
          <li><Link to="/">Home</Link></li>
          <li className="active"><span>Charts</span></li>
        </ul>

        <div className="page-header">
          <h1>Charts</h1>
        </div>

        <div>
          {charts.length ? charts.map((chart, index) => (
            <div className="row" key={index}>
              <div className="col-sm-3">
                <div style={{ width: '100%', height: 180 }} ref={chartMap[index]}>Loading...</div>
              </div>
              <div className="col-sm-8">
                <h2><Link to={`/charts/${chartMap[index]}`}>{chartMap[index]}</Link></h2>
                <div>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </div>
            </div>
          )) : (
            <div>Loading...</div>
          )}
        </div>
      </div>
    )
  }

  renderChart(target, data) {
    this.state.chartInstances.push(
      new Highcharts.Chart(target, {
        title: { text: null },
        legend: { enabled: false },
        credits: { enabled: false },
        yAxis: { visible: false },
        xAxis: {
          type: 'datetime',
          visible: false
        },
        plotOptions: {
          series: {
            states: {
              hover: {
                enabled: false,
              }
            }
          }
        },
        tooltip: {
          enabled: false
        },
        series: [{ data: data, lineWidth: 1 }]
      })
    )
  }
}

Charts.propTypes = {
  loadCharts: PropTypes.func.isRequired,
  charts: PropTypes.arrayOf(PropTypes.array).isRequired
}

export default connect(
  state => ({
    charts: state.charts.charts
  }),

  dispatch => bindActionCreators({
    loadCharts
  }, dispatch)
)(Charts)
