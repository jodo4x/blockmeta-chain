import React from 'react'

if (process.env.BROWSER) {
  require('./index.scss')
}

export default function About() {
  return (
    <div className="container">
      <div className="page-header">
        <h1>About</h1>
      </div>
      <div>...</div>
    </div>
  )
}
