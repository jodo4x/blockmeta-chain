import React, { PropTypes, Component } from 'react'
import bindActionCreators from 'redux/lib/bindActionCreators'
import Highcharts from 'highcharts'
import h3d from 'highcharts/highcharts-3d'
import connect from 'react-redux/lib/components/connect'
import Link from 'react-router/lib/Link'
import loadArchives from '../../actions/archives'
import digits from '../../lib/digits'
import Dropdown from 'react-bootstrap/lib/Dropdown'
import MenuItem from 'react-bootstrap/lib/MenuItem'
import Pagination from 'react-bootstrap/lib/Pagination'
import chartColors from '../../constants/chartColors'
import btcAddrCats from '../../constants/btcAddrCats'

if (process.env.BROWSER) {
  require('./index.scss')
}

class Archives extends Component {
  static fetchData({ store }) {
    return store.dispatch(loadArchives())
  }

  constructor() {
    super()
    this.state = {
      activeCat: 'all'
    }
    this.handlePagination = this.handlePagination.bind(this)
    this.handleCategory = this.handleCategory.bind(this)
  }

  componentDidMount() {
    this.props.loadArchives()
    .then(({ type, payload }) => {
      const { stat } = payload
      const { total_count, archive_dist } = stat

      const chartData = Object.keys(archive_dist).map((key) => ({
        name: `${btcAddrCats[key]} ${(archive_dist[key]/total_count*100).toFixed(2)}%`,
        y: archive_dist[key]
      }))

      h3d(Highcharts)

      this.chart = new Highcharts.Chart(this.refs.chart, {
        title: { text: null },
        colors: chartColors,
        tooltip: { shadow: false },
        plotOptions: {
          pie: {
            depth: 25, // enable 3d
          }
        },
        chart: {
          type: 'pie',
          options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
          }
        },
        series: [{
          name: '地址数',
          type: "pie",
          size: '60%',
          innerSize: '47%',
          data: chartData
        }],
        credits: { enabled: false }
      })
    })
  }

  componentWillUnmount() {
    this.chart &&
    this.chart.destroy()
  }

  render() {
    const { activeCat } = this.state
    const { stat, archives, no_page, pages } = this.props
    const { archive_dist } = stat

    return (
      <div className="container">
        <div className="archives">
          <div className="alert alert-warning">目前只支持<strong>比特币</strong>相关的统计信息，关于<em>以太坊<a className="alert-link" href="https://www.ethereum.org/" target="_blank">(分叉)</a></em>和<em>以太坊<sub><a className="alert-link" href="https://ethereumclassic.github.io/" target="_blank">(原链)</a></sub></em>暂时不支持。</div>
          <div className="addr-dist">
            <h2>地址分布</h2>
            <div className="text-muted">(以下展示所有链上地址的分布情况)</div>

            <div className="row">
              <div className="col-sm-6">
                <div style={{ width: '100%', height: 300 }} ref="chart">正在加载...</div>
              </div>
              <div className="col-sm-3">
                <h3>概览</h3>
                <dl className="dl-horizontal">
                  <dt>所有地址:</dt>
                  <dd style={{ marginBottom: 5 }}>{digits(stat.total_count) || '...'}</dd>
                  <dt>验证地址:</dt>
                  <dd>{digits(stat.verified_count) || '...'}</dd>
                  <dt>未验证地址:</dt>
                  <dd>
                    {stat.total_count
                      && stat.verified_count
                      && digits(stat.total_count - stat.verified_count)}
                  </dd>
                </dl>
              </div>
              <div className="col-sm-3">
                <h3>目录</h3>
                {archive_dist && (
                  Object.keys(archive_dist).map((key, index) => (
                    <dl className="dl-horizontal" key={index}>
                      <dt>{btcAddrCats[key]}:</dt>
                      <dd>{digits(archive_dist[key])}</dd>
                    </dl>
                  ))
                )}
              </div>
            </div>
          </div>

          <div className="addr-list">
            <h2>地址列表</h2>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th className="td-v">是否认证</th>
                  <th className="td-a">地址</th>
                  <th className="td-t">标签</th>
                  <th className="td-c text-capitalize">
                    <Dropdown
                      onSelect={(key) => this.handleCategory(key)}
                      id="id__category-dropdown"
                    >
                      <Dropdown.Toggle>
                        <span>目录:<strong className="active-cat">{btcAddrCats[activeCat]}</strong></span>
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        {archive_dist &&
                          ['all', ...Object.keys(archive_dist)].map((key, index) => (
                            <MenuItem
                              eventKey={key}
                              key={index}
                            >
                              {btcAddrCats[key]}
                            </MenuItem>
                          ))}
                      </Dropdown.Menu>
                    </Dropdown>
                  </th>
                </tr>
              </thead>
              <tbody>
                {archives.map((arch, index) => (
                  <tr className={arch.verified ? 'tr-verified' : 'tr-unverified'} key={index}>
                    <td className="td-v">
                      {arch.verified ? (
                        <span className="v-symbol text-success">&#x2714;</span>
                      ) : (
                        <span className="v-symbol text-warning">&#x2718;</span>
                      )}
                    </td>
                    <td className="td-a"><Link to={`/address/${arch.address}?chain=bitcoin`}><code>{arch.address}</code></Link></td>
                    <td className="td-t">
                      {arch.link ? (
                        <span>
                          <a className="text-muted" href={arch.link} target="_blank">{arch.tag}</a>
                          <sup><i className="fa fa-external-link" /></sup>
                        </span>
                      ) : (
                        <span>{arch.tag}</span>
                      )}
                    </td>
                    <td className="td-c text-capitalize">
                      <span className="click-trigger" onClick={() => this.handleCategory(arch.category)}>
                        {btcAddrCats[arch.category]}
                      </span>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <div className="pager-container">
              <Pagination
                prev
                next
                maxButtons={10}
                activePage={no_page}
                items={pages}
                onSelect={this.handlePagination}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }

  handleCategory(cat) {
    this.props.loadArchives('bitcoin', 1, cat)
    .then(({ type, payload }) => {
      this.setState({
        activeCat: cat
      })
    })
  }

  handlePagination(page) {
    this.props.loadArchives('bitcoin', page)
  }
}

Archives.propTypes = {
  loadArchives: PropTypes.func.isRequired,
  stat: PropTypes.object.isRequired,
  archives: PropTypes.array.isRequired,
  no_page: PropTypes.number.isRequired,
  pages: PropTypes.number.isRequired
}

export default connect(
  state => state.archives,

  dispatch => bindActionCreators({
    loadArchives
  }, dispatch)
)(Archives)
