import React, { PropTypes, Component } from 'react'
import bindActionCreators from 'redux/lib/bindActionCreators'
import connect from 'react-redux/lib/components/connect'
import Navbar from '../Navbar'
import Footer from '../Footer'
import ErrorMsg from '../ErrorMsg'
import { loadSearch } from '../../actions/homepage'
import { toggleDisplayStyle } from '../../actions/ui'
import cookie from 'cookie'

if (process.env.BROWSER) {
  require('./index.scss')
}

class Layout extends Component {
  constructor() {
    super()
    this.handleSearch = this.handleSearch.bind(this)
  }

  componentDidMount() {
    const { displayStyle } = cookie.parse(document.cookie)

    if (!displayStyle) return

    const { toggleDisplayStyle, ui } = this.props

    toggleDisplayStyle(
      /normal|wide/i.test(displayStyle) ? // 检测cookie是否被恶意改写
      displayStyle :
      ui.displayStyle
    )
  }

  render() {
    const {
      ui: { displayStyle },
      location,
      children,
      toggleDisplayStyle
    } = this.props

    const container = {
      'normal': 'container',
      'wide': 'container-fluid'
    }

    return (
      <div className="app">

        <Navbar
          pathname={location.pathname}
          handleSearch={this.handleSearch}
        />

        <div className={'container-fluid'/*container[displayStyle]*/}>
          <ErrorMsg />
          {children}
        </div>

        <Footer
          data={{ displayStyle }}
          toggleDisplayStyle={toggleDisplayStyle}
        />
      </div>
    )
  }

  handleSearch(e, value) {
    e.preventDefault()
    this.props.loadSearch('bitcoin', value)
  }
}

Layout.propTypes = {
  loadSearch: PropTypes.func.isRequired,
  toggleDisplayStyle: PropTypes.func.isRequired,
  'location': PropTypes.object.isRequired,
  children: PropTypes.element.isRequired,
  ui: PropTypes.shape({
    displayStyle: PropTypes.string.isRequired
  }).isRequired
}

export default connect(
  state => ({
    ui: state.ui
  }),
  dispatch => bindActionCreators({
    loadSearch,
    toggleDisplayStyle
  }, dispatch)
)(Layout)
