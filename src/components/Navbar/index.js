import React, { PropTypes } from 'react'
import Link from 'react-router/lib/Link'
import Dropdown from 'react-bootstrap/lib/Dropdown'
import MenuItem from 'react-bootstrap/lib/MenuItem'

if (process.env.BROWSER) {
  require('./index.scss')
}

export default function Navbar({ pathname, handleSearch }) {
  let kw

  return (
    <div className={`navbar navbar-${pathname === '/' ? 'default' : 'primary'}`}>
      <div className="container">
        <div className="navbar-header">
          <Link className="navbar-brand" to="/"><strong>block</strong>meta</Link>
        </div>
        <div className="collapse navbar-collapse">
          <ul className="nav navbar-nav text-capitalize">
            <li className={pathname === '/stats' ? 'active': ''}>
              <Link to="/stats">统计</Link>
            </li>
            {/*
            <li className={pathname === '/charts' ? 'active': ''}>
              <Link to="/charts">图表</Link>
            </li>
            */}
            <li className={pathname === '/archives' ? 'active': ''}>
              <Link to="/archives">档案</Link>
            </li>
            {/*
            <li className={pathname === '/nodes' ? 'active': ''}>
              <Link to="/nodes">节点</Link>
            </li>
            */}
            <li className={pathname === '/api' ? 'active' : ''}>
              <Link to="/api">API</Link>
            </li>
          </ul>
          <ul className="nav navbar-nav navbar-right">
            <li>
              <Dropdown id="id__lang">
                <Dropdown.Toggle className="navbar-btn">中文(简体)</Dropdown.Toggle>
                <Dropdown.Menu>
                  <MenuItem>English</MenuItem>
                  <MenuItem>中文(简体)</MenuItem>
                  <MenuItem>中文(正體)</MenuItem>
                  <MenuItem>日本語</MenuItem>
                </Dropdown.Menu>
              </Dropdown>
            </li>
          </ul>
          <form className="navbar-form navbar-right" onSubmit={(e) => handleSearch(e, kw.value)}>
            <div className="form-group">
              <input className="form-control" type="text" placeholder="Address/height/hash..." ref={n => kw = n} />
            </div>
            <button className="btn btn-default" type="submit"><i className="fa fa-search" /></button>
          </form>
        </div>
      </div>
    </div>
  )
}

Navbar.propTypes = {
  pathname: PropTypes.string.isRequired
}
