import React, { PropTypes, Component } from 'react'
import moment from 'moment'
import Link from 'react-router/lib/Link'
import qr from 'qr-image'
import Pagination from 'react-bootstrap/lib/Pagination'
import Price from '../Price'
import TxSwitcher from '../TxSwitcher'
import digits from '../../lib/digits'
import Nav from 'react-bootstrap/lib/Nav'
import NavDropdown from 'react-bootstrap/lib/NavDropdown'
import NavItem from 'react-bootstrap/lib/NavItem'
import MenuItem from 'react-bootstrap/lib/MenuItem'

import 'moment/locale/zh-cn'
moment.locale('zh')

export default class BitcoinAddress extends Component {
  constructor() {
    super()

    this.state = {
      activeKey: 'all',
      filters: {
        'all': '全部',
        'confirmed': '已确认',
        'unconfirmed': '未确认',
        'recv': '接收',
        'sent': '发送',
        'unspent': '未花费'
      }
    }

    ;[
      'handleSelect',
      'renderTx',
      'renderTxHorizontal',
      'renderTxVertical',
      'renderAddr'
    ].forEach((handler) => {
      this[handler] = this[handler].bind(this)
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      const { addr } = nextProps.data.address
      if (addr) {
        this.refs.qrcode.innerHTML = qr.imageSync(addr, {
          type: 'svg',
          size: 7
        })
      }
    }
  }

  render() {
    const { activeKey, filters } = this.state
    const {
      data: {
        chain,
        address,
        txSwitchType
      },
      handlePagination
    } = this.props

    return (
      <div className="content">

        {this.renderSumm(address, chain)}

        <div className="panel-txs">

          <Nav bsStyle="tabs" activeKey={activeKey} onSelect={this.handleSelect}>
            <NavItem eventKey="all">全部</NavItem>
            <NavDropdown eventKey="confirmed" title="已确认" id="id__dropdown">
              <MenuItem eventKey="confirmed.confirmed">全部</MenuItem>
              <MenuItem eventKey="confirmed.recv">接收</MenuItem>
              <MenuItem eventKey="confirmed.sent">发送</MenuItem>
              <MenuItem eventKey="confirmed.unspent">未花费</MenuItem>
            </NavDropdown>
            <NavItem eventKey="unconfirmed">未确认</NavItem>
          </Nav>

          {address.txs.length ? (
            <div>

              {address.txs.map((tx, index) => (
                <div className="panel panel-default panel-tx" key={index}>
                  <div className="panel-heading">
                    <div className="panel-title clearfix">
                      <div className="pull-right tx-time">{moment(tx.tx_time*1000).format('YYYY-MM-DD HH:mm:ss')}</div>
                      <div>
                        {tx.is_coinbase === 1 && (
                          <span
                            className="label label-success"
                            style={{ marginRight: 5 }}
                          >
                            <span>COINBASE</span>
                          </span>
                        )}
                        <Link to={`/tx/${tx.hash}?chain=${chain}`}><code>{tx.hash}</code></Link>
                      </div>
                    </div>
                  </div>
                  <div className="panel-body" style={{ position: 'relative' }}>

                    {this.renderTx(tx, chain, txSwitchType)}

                  </div>
                </div>
              ))}

              <div className="pager-container">
                <div>总共 {address.pages} 页 {address.tx_num} 个交易</div>
                <Pagination
                  prev
                  next
                  maxButtons={10}
                  activePage={address.no_page}
                  items={address.pages}
                  onSelect={handlePagination}
                />
              </div>

            </div>
          ) : (
            <div className="alert alert-warning">没有交易.</div>
          )}
        </div>

      </div>
    )
  }

  handleSelect(eventKey) {
    let match
    let filterKey
    let activeKey

    if (eventKey.indexOf('.') > -1) {
      match = eventKey.match(/([a-zA-Z]*)\.([a-zA-Z]*)/)
      filterKey = match[2]
      activeKey = match[1]
    } else {
      filterKey = activeKey = eventKey
    }

    this.props.handleSelect(filterKey)
    .then(({ type, payload }) => {
      this.setState({
        activeKey: activeKey
      })
    })
  }

  renderSumm(address, chain) {
    return (
      <div className="summ">
        <div className="row">
          <div className="col-sm-5">
            <div className="text-center qrcode" ref="qrcode" style={{ marginTop: 30 }}>正在加载...</div>
          </div>
          <div className="col-sm-7">
            <div className="text-center balance">
              <strong>余额:</strong>
              <span className="lead" style={{ fontSize: 30 }}>
                <Price value={address.balance} />
                <em className="text-muted">BTC</em>
              </span>
            </div>
            <table className="table table-striped">
              <tbody>
                <tr>
                  <th>地址</th>
                  <td><span className="hashcode">{address.addr}</span></td>
                </tr>
                <tr>
                  <th>接收</th>
                  <td><Price value={address.recv} /><em className="text-muted">BTC</em></td>
                </tr>
                <tr>
                  <th>发送</th>
                  <td><Price value={address.sent} /><em className="text-muted">BTC</em></td>
                </tr>
                <tr>
                  <th>hash160</th>
                  <td><span className="hashcode">{address.hash160}</span></td>
                </tr>
                <tr>
                  <th>交易数</th>
                  <td>{address.tx_num}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }

  renderTx(tx, chain, txSwitchType) {
    return ({
      'h': this.renderTxHorizontal,
      'v': this.renderTxVertical
    })[txSwitchType](tx, chain)
  }

  renderTxHorizontal(tx, chain) {
    return (
      <div>

        <div className="row tx-item">
          <div className="col-sm-5 addr-list-container">
            {tx.is_coinbase === 1 ? (
              <span>新生块奖励</span>
            ) : (
              <ul className="addr-list list-unstyled addr-out">
                {tx.inputs.map((tx, index) => (
                  <li key={index}>
                    <span className="pull-right"><Price value={tx.amount} /><em className="text-muted">BTC</em></span>
                    {this.renderAddr(tx.addr)}
                  </li>
                ))}
              </ul>
            )}
          </div>
          <div className="col-sm-2 text-center tx-arrow">
            <span className="lead" style={{ fontFamily: 'sans-serif' }}>&rarr;</span>
          </div>
          <div className="col-sm-5 addr-list-container">
            <ul className="addr-list list-unstyled">
              {tx.outputs.map((tx, index) => (
                <li key={index}>
                  <span className="pull-right"><Price value={tx.amount} /><em className="text-muted">BTC</em></span>
                  {this.renderAddr(tx.addr)}
                </li>
              ))}
            </ul>
          </div>
        </div>

        <hr className="hidden" />

        <div className="row">
          <div className="col-sm-6">
            <table className="table table-striped table-txinfo hidden">
              <tbody>
                <tr>
                  <th>区块高度</th>
                  <td><Link to={`/block/${tx.block_height}?chain=${chain}`}>{digits(tx.block_height)}</Link></td>
                  <th>确认数</th>
                  <td>{tx.confirmation}</td>
                </tr>
                <tr>
                  <th>总接收</th>
                  <td><Price value={tx.total_input} /><em className="text-muted">BTC</em></td>
                  <th>总发送</th>
                  <td><Price value={tx.total_output} /><em className="text-muted">BTC</em></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="col-sm-6">
            <div className="text-right">
              <div className={`lead ${/\-/.test(tx.tx_value) ? 'text-danger' : ''}`} style={{ fontSize: 30, marginBottom: 0 }}>
                <Price value={tx.tx_value} />
                <small className="text-muted">BTC</small>
              </div>
              <div className={`span-fee ${/\-/.test(tx.tx_value) ? 'text-danger' :''}`}><strong>手续费: </strong><Price value={tx.fee} /><em className="text-muted">BTC</em></div>
            </div>
          </div>
        </div>

      </div>
    )
  }

  renderTxVertical(tx, chain) {
    return (
      <div className="row">
        <div className="col-sm-7">
          <div className="addr-list-container">
            <dl className="addr-list addr-list-v">
              <dt>
                <ul className="list-unstyled">
                  {tx.inputs.map((tx, index) => (
                    <li key={index}>
                      <span className="pull-right"><Price value={tx.amount} /><em className="text-muted">BTC</em></span>
                      {this.renderAddr(tx.addr)}
                    </li>
                  ))}
                </ul>
              </dt>
              <dd style={{ paddingLeft: 10, position: 'relative'  }}>
                <ul className="list-unstyled">
                  {tx.outputs.map((tx, index) => (
                    <li key={index}>
                      <span className="pull-right"><Price value={tx.amount} /><em className="text-muted">BTC</em></span>
                      {this.renderAddr(tx.addr)}
                    </li>
                  ))}
                </ul>
              </dd>
            </dl>
          </div>
        </div>
        <div className="col-sm-5 text-center">
          <div className="amount">
            <div>
              <div className="lead" style={{ fontSize: 30, marginBottom: 0 }}>
                <Price value={tx.tx_value} />
                <small className="text-muted">BTC</small>
              </div>
              <div className="text-muted">(总额度)</div>
            </div>
            <div>花费 <Price value={tx.fee} /><em className="text-muted">BTC</em></div>
          </div>
          <table className="table table-striped table-txinfo text-left hidden">
            <tbody>
              <tr>
                <th>区块高度</th>
                <td><Link to={`/block/${tx.block_height}?chain=${chain}`}>{digits(tx.block_height)}</Link></td>
              </tr>
              <tr>
                <th>确认数</th>
                <td>{tx.confirmation}</td>
              </tr>
              <tr>
                <th>总接收</th>
                <td><Price value={tx.total_input} /><em className="text-muted">BTC</em></td>
              </tr>
              <tr>
                <th>总发送</th>
                <td><Price value={tx.total_output} /><em className="text-muted">BTC</em></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }

  renderAddr(addr) {
    const { data: { chain, address } } = this.props

    return address.addr === addr ? (
      <span className="pull-left"><code>{addr}</code></span>
    ) : (
      <Link className="pull-left" to={`/address/${addr}?chain=${chain}`}><code>{addr}</code></Link>
    )
  }
}

BitcoinAddress.propTypes = {
  handleSelect: PropTypes.func.isRequired,
  handlePagination: PropTypes.func.isRequired,
  data: PropTypes.shape({
    chain: PropTypes.string.isRequired,
    address: PropTypes.shape({
      txs: PropTypes.array.isRequired
    }).isRequired,
    txSwitchType: PropTypes.string.isRequired
  }).isRequired
}
