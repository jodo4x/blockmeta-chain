import React, { PropTypes, Component } from 'react'
import bindActionCreators from 'redux/lib/bindActionCreators'
import connect from 'react-redux/lib/components/connect'
import loadAddress from '../../actions/address'
import Link from 'react-router/lib/Link'
import chainPattern from '../../constants/chainPattern'

import BitcoinAddress from './BitcoinAddress'
import EthereumAddress from './EthereumAddress'

if (process.env.BROWSER) {
  require('./index.scss')
}

class Address extends Component {
  static fetchData({ store, location, params }) {
    return store.dispatch(loadAddress(location.query.chain, params.id))
  }

  constructor() {
    super()

    this.state = {
      hasCorrectChainName: false
    }

    ;['handleSelect', 'handlePagination'].forEach((handler) => {
      this[handler] = this[handler].bind(this)
    })
  }

  componentWillMount() {
    if (chainPattern.test(this.props.location.query.chain)) {
      this.setState({
        hasCorrectChainName: true
      })
    }
  }

  componentDidMount() {
    this.loadAddress(this.props.params.id)
  }

  componentWillReceiveProps(nextProps) {
    const { params } = nextProps

    if (this.props.params.id !== params.id) {
      this.loadAddress(params.id)
    }
  }

  render() {
    const { hasCorrectChainName } = this.state
    const {
      'location': { query: { chain } },
      params,
      address
    } = this.props
    const addr = chain === 'bitcoin' ? address.addr : address.address
    const contractFlag = address.contract_flag ? '合约' : ''

    return hasCorrectChainName ? (
      <div className="container">
        <div className="addr">
          <ul className="breadcrumb">
            <li><Link to="/">首页</Link></li>
            <li><span>{contractFlag}地址</span></li>
            <li className="active">
              <span>{addr}</span>
            </li>
          </ul>
          <div className="page-header">
            <h1><span className="l">{contractFlag}地址:</span>{addr}<small className="text-muted">({chain})</small></h1>
          </div>

          {this.renderChild(chain)}
        </div>
      </div>
    ) : null
  }

  loadAddress(id) {
    const { location, loadAddress } = this.props
    loadAddress(location.query.chain, id)
  }

  renderChild(chain) {
    const { address, txSwitchType } = this.props

    let child
    switch(chain) {
      case 'bitcoin':
        return child = (
          <BitcoinAddress
            data={{ chain, address, txSwitchType }}
            handleSelect={this.handleSelect}
            handlePagination={this.handlePagination}
          />
        )
      case 'ethereum':
      case 'ethereumclassic':
        return child = (
          <EthereumAddress
            data={{ chain, address, txSwitchType }}
            handlePagination={this.handlePagination}
          />
        )
      default:
        return child
    }
  }

  // NOTE: 只有比特币才有
  handleSelect(eventKey) {
    const { location, params, loadAddress } = this.props
    return loadAddress(location.query.chain, params.id, 1, eventKey)
  }

  handlePagination(page) {
    const { location, params, loadAddress } = this.props
    loadAddress(location.query.chain, params.id, page)
  }
}

Address.propTypes = {
  loadAddress: PropTypes.func.isRequired,
  address: PropTypes.shape({
    txs: PropTypes.array.isRequired
  }).isRequired,
  txSwitchType: PropTypes.string.isRequired
}

export default connect(
  state => ({
    address: state.address,
    txSwitchType: state.ui.txSwitchType
  }),

  dispatch => bindActionCreators({ loadAddress }, dispatch)
)(Address)
