import React, { PropTypes, Component } from 'react'
import moment from 'moment'
import qr from 'qr-image'
import Link from 'react-router/lib/Link'
import Pagination from 'react-bootstrap/lib/Pagination'
import Price from '../Price'
import TxSwitcher from '../TxSwitcher'
import digits from '../../lib/digits'

import 'moment/locale/zh-cn'
moment.locale('zh')

export default class EthereumAddress extends Component {
  constructor(props) {
    super()

    this.state = {
      unit: props.data.chain === 'ethereum' ? 'ETH' : 'ETC'
    }

    ;[
      'renderSumm',
      'renderTx',
      'renderTxHorizontal',
      'renderTxVertical',
      'renderAddr',
    ].forEach((handler) => {
      this[handler] = this[handler].bind(this)
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      const { address } = nextProps.data.address
      if (address) {
        this.refs.qrcode.innerHTML = qr.imageSync(address, {
          type: 'svg',
          size: 7
        })
      }
    }
  }

  render() {
    const { unit } = this.state
    const {
      data: {
        chain,
        address,
        txSwitchType
      },
      handlePagination
    } = this.props

    return (
      <div className="content">
        {this.renderSumm(address, chain, unit)}

        <div className="panel-txs">
          {address.tx.length ? (
            <div>

              {address.tx.map((tx, index) => (
                <div className="panel panel-default panel-tx" key={index}>
                  <div className="panel-heading">
                    <div className="panel-title clearfix">
                      <div className="pull-right tx-time">{moment(tx.timestamp*1000).format('YYYY-MM-DD HH:mm:ss')}</div>
                      <div><Link to={`/tx/${tx.hash}?chain=${chain}`}><code>{tx.hash}</code></Link></div>
                    </div>
                  </div>
                  <div className="panel-body">
                    {this.renderTx(tx, chain, unit, txSwitchType)}
                  </div>
                </div>
              ))}

              <div className="pager-container">
                <div>总共 {address.pages} 页 {address.tx_num} 个交易</div>
                <Pagination
                  prev
                  next
                  maxButtons={10}
                  activePage={address.no_page}
                  items={address.pages}
                  onSelect={handlePagination}
                />
              </div>

            </div>
          ) : (
            <div className="alert alert-warning">没有交易.</div>
          )}
        </div>

      </div>
    )
  }

  renderSumm(address, chain, unit) {
    return (
      <div className="summ">
        <div className="row">
          <div className="col-sm-5">
            <div className="text-center qrcode" ref="qrcode">正在加载...</div>
          </div>
          <div className="col-sm-7">
            <div className="text-center">
              <div>
                <strong style={{ marginRight: 5 }}>余额:</strong>
                <span className="lead" style={{ fontSize: 30 }}>
                  {address.balance ? <Price value={address.balance} /> : '0'}
                  <em className="text-muted">{unit}</em>
                </span>
              </div>
            </div>
            <table className="table table-striped">
              <tbody>
                <tr>
                  <th>地址</th>
                  <td><span className="hashcode">{address.address}</span></td>
                </tr>
                <tr>
                  <th>交易数</th>
                  <td>{digits(address.tx_num)}</td>
                </tr>
                <tr>
                  <th>代币监控</th>
                  <td>
                    {address.token_flags.map((token, index) => (
                      <Link to={`/token/${token}?chain=${chain}`} key={index}>{token}</Link>
                    ))}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }

  renderTx(tx, chain, unit, txSwitchType) {
    return ({
      h: this.renderTxHorizontal,
      v: this.renderTxVertical
    })[txSwitchType](tx, chain, unit)
  }

  renderTxHorizontal(tx, chain, unit) {
    return (
      <div>

        <div className="row tx-item">
          <div className="col-sm-5">
            {this.renderAddr(tx.from)}
          </div>
          <div className="col-sm-2 text-center tx-arrow">
            <span className="lead" style={{ fontFamily: 'sans-serif' }}>&rarr;</span>
          </div>
          <div className="col-sm-5">
            {this.renderAddr(tx.to)}
          </div>
        </div>

        <hr className="hidden" />

        <div className="row">
          <div className="col-sm-6">
            <table className="table table-striped table-txinfo text-left hidden">
              <tbody>
                <tr>
                  <th>块高度</th>
                  <td><Link to={`/block/${tx.blockNumber}?chain=${chain}`}>{digits(tx.blockNumber)}</Link></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="col-sm-6">
            <div className="text-right">
              <div className={`lead ${/\-/.test(tx.value) ? 'text-danger' : ''}`} style={{ fontSize: 30, marginBottom: 0 }}>
                <Price value={tx.value} />
                <small className="text-muted">{unit}</small>
              </div>
              <div className={`span-fee ${/\-/.test(tx.value) ? 'text-danger' : ''}`}><strong>手续费: </strong><Price value={tx.fee} /><em className="text-muted">{unit}</em></div>
            </div>
          </div>
        </div>

      </div>
    )
  }

  renderTxVertical(tx, chain, unit) {
    return (
      <div className="row">
        <div className="col-sm-7">
          <div className="addr-list-container">
            <dl className="addr-list addr-list-v">
              <dt>
                <ul className="list-unstyled">
                  <li>
                    {this.renderAddr(tx.from)}
                  </li>
                </ul>
              </dt>
              <dd style={{ paddingLeft: 10, position: 'relative'  }}>
                <ul className="list-unstyled">
                  <li>
                    {this.renderAddr(tx.to)}
                  </li>
                </ul>
              </dd>
            </dl>
          </div>
        </div>
        <div className="col-sm-5 text-center">
          <div className="amount">
            <div>
              <div className="lead" style={{ fontSize: 30, marginBottom: 0 }}>
                <Price value={tx.value} />
                <small className="text-muted">{unit}</small>
              </div>
              <div className="text-muted">(总额度)</div>
            </div>
            <div className="v-fee">手续费: <Price value={tx.fee} /><em className="text-muted">{unit}</em></div>
          </div>
          <table className="table table-striped table-txinfo text-left hidden">
            <tbody>
              <tr>
                <th>块高度</th>
                <td><Link to={`/block/${tx.blockNumber}?chain=${chain}`}>{digits(tx.blockNumber)}</Link></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }

  renderAddr(addr) {
    const { data: { chain, address } } = this.props
    return address.address === addr ? (
      <span><code>{addr}</code></span>
    ) : (
      <Link to={`/address/${addr}?chain=${chain}`}><code>{addr}</code></Link>
    )
  }
}

EthereumAddress.propTypes = {
  handlePagination: PropTypes.func.isRequired,
  data: PropTypes.shape({
    chain: PropTypes.string.isRequired,
    address: PropTypes.object.isRequired,
    txSwitchType: PropTypes.string.isRequired
  }).isRequired
}
