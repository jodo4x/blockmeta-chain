import React, { PropTypes, Component } from 'react'
import bindActionCreators from 'redux/lib/bindActionCreators'
import connect from 'react-redux/lib/components/connect'
import Link from 'react-router/lib/Link'
import { parseParams } from '../../actions/charts'
import loadChart from '../../actions/chart'
import digits from '../../lib/digits'
import Highcharts from 'highcharts'

class Chart extends Component {
  static fetchData({ store, location, params }) {
    const parsed = parseParams(params)
    return store.dispatch(
      loadChart(parsed[1], 'bitcoin', parsed[2], 365)
    )
  }

  constructor() {
    super()
  }

  componentDidMount() {
    const parsed = parseParams(this.props.params)
    this.props.loadChart(parsed[1], 'bitcoin', parsed[2], 365)
    .then(({ type, payload }) => {
      const { chart } = payload

      this.chart = new Highcharts.Chart(this.refs.chart, {
        chart: {
          type: 'area'
        },
        yAxis: {
          title: { text: null },
          labels: {
            formatter: function() { return  digits(this.value) }
          }
        },
        xAxis: {
          type: 'datetime'
        },
        series: [{
          data: chart.map((data) => ([
            data[0]*1000, parseFloat(data[1])
          ]))
        }]
      })
    })
  }

  componentWillUnmount() {
    this.chart &&
    this.chart.destroy()
  }

  render() {
    const {
      params,
      chart
    } = this.props

    return (
      <div className="container">
        <ul className="breadcrumb">
          <li><Link to="/">Home</Link></li>
          <li><Link to="/charts">Charts</Link></li>
          <li className="active"><span>{params.chart}</span></li>
        </ul>
        <div className="page-header text-center">
          <h1>{params.chart}</h1>
        </div>
        <div>
          <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </div>
          <div style={{ width: '100%', height: 400 }} ref="chart">Loading...</div>
        </div>
      </div>
    )
  }
}

Chart.propTypes = {
  loadChart: PropTypes.func.isRequired
}

export default connect(
  state => ({
    chart: state.chart
  }),

  dispatch => bindActionCreators({
    loadChart
  }, dispatch)
)(Chart)
