import React from 'react'

export default function NotFound() {
  return (
    <div style={{ marginTop: 20 }}>
      <div className="page-header text-center">
        <h1>页面不存在</h1>
      </div>
    </div>
  )
}
