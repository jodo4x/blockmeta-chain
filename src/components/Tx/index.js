import React, { PropTypes, Component } from 'react'
import bindActionCreators from 'redux/lib/bindActionCreators'
import connect from 'react-redux/lib/components/connect'
import Link from 'react-router/lib/Link'
import digits from '../../lib/digits'
import chainPattern from '../../constants/chainPattern'

import loadTx from '../../actions/tx'
import BitcoinTx from './BitcoinTx'
import EthereumTx from './EthereumTx'

if (process.env.BROWSER) {
  require('./index.scss')
}

class Tx extends Component {
  static fetchData({ store, location, params }) {
    return store.dispatch(loadTx(location.query.chain, params.id))
  }

  constructor() {
    super()
    this.state = {
      hasCorrectChainName: false
    }
  }

  componentWillMount() {
    if (chainPattern.test(this.props.location.query.chain)) {
      this.setState({
        hasCorrectChainName: true
      })
    }
  }

  componentDidMount() {
    const { location, params } = this.props
    this.props.loadTx(location.query.chain, params.id)
  }

  render() {
    const { hasCorrectChainName } = this.state
    const {
      'location': { query },
      params,
      tx
    } = this.props

    return hasCorrectChainName ? (
      <div className="container">
        <div className="tx">
          <ul className="breadcrumb">
            <li><Link to="/">首页</Link></li>
            <li><span>交易</span></li>
            <li className="active"><span>{params.id}</span></li>
          </ul>

          <div className="page-header">
            <h1><span className="l">交易:</span><span>{params.id}</span></h1>
          </div>

          {this.renderChild(query.chain)}
        </div>
      </div>
    ) : null
  }

  renderChild(chain) {
    const { tx } = this.props

    let child
    switch(chain) {
      case 'bitcoin':
        return child = (
          <BitcoinTx
            data={{ chain, tx }}
          />
        )
      case 'ethereum':
      case 'ethereumclassic':
        return child = (
          <EthereumTx
            data={{ chain, tx }}
          />
        )
      default:
        return child
    }
  }
}

Tx.propTypes ={
  loadTx: PropTypes.func.isRequired,
  tx: PropTypes.shape({
    inputs: PropTypes.array.isRequired,
    outputs: PropTypes.array.isRequired,
    token_txs: PropTypes.array.isRequired,
    tx: PropTypes.object.isRequired,
  }).isRequired
}

export default connect(
  state => ({
    tx: state.tx
  }),

  dispatch => bindActionCreators({
    loadTx
  }, dispatch)
)(Tx)
