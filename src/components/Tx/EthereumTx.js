import React, { PropTypes, Component } from 'react'
import Link from 'react-router/lib/Link'
import moment from 'moment'
import digits from '../../lib/digits'
import Price from '../Price'
import Accordion from 'react-bootstrap/lib/Accordion'
import Panel from 'react-bootstrap/lib/Panel'

import 'moment/locale/zh-cn'
moment.locale('zh')

export default class EthereumTx extends Component {
  constructor() {
    super()

    this.state = {
      selected: 'tkn',
      tabs: {
        tkn: '代币交易',
        eth: '原始数据'
      }
    }

    ;[
      'handleSelect',
      'renderSumm',
      'renderTx',
      'renderTknTx',
      'renderLogs',
      'renderSub'
    ].forEach((handler) => {
      this[handler] = this[handler].bind(this)
    })
  }

  render() {
    const { selected, tabs } = this.state
    const {
      data: { chain, tx: { token_txs, tx } }
    } = this.props

    const unit = chain === 'ethereum' ? 'ETH' : 'ETC'

    return (
      <div className="content">
        {this.renderSumm(tx, chain, unit)}

        {this.renderTx(tx, chain, unit)}

        <div>
          <h2 style={{ fontSize: 20, marginBottom: 20 }}>日志</h2>

          <ul className="nav nav-tabs">
            {Object.keys(tabs).map((key, index) => (
              <li className={selected === key ? 'active' : ''} key={index}>
                <a href onClick={(e) => this.handleSelect(e, key)}>{tabs[key]}</a>
              </li>
            ))}
          </ul>

          {this.renderSub({ token_txs, tx, chain, unit })}
        </div>

      </div>
    )
  }

  handleSelect(e, key) {
    e.preventDefault()
    this.setState({
      selected: key
    })
  }

  renderSumm(tx, chain, unit) {
    return (
      <div>
        <div className="row">
          <div className="col-sm-6">
            <table className="table table-striped">
              <tbody>
                <tr>
                  <th>交易额</th>
                  <td><Price value={tx.value} /><em className="text-muted">{unit}</em></td>
                </tr>
                <tr>
                  <th>手续费</th>
                  <td>{tx.fee}</td>
                </tr>
                <tr>
                  <th>时间戳</th>
                  <td>{moment(tx.timestamp*1000).fromNow()}</td>
                </tr>
                <tr>
                  <th>所属区块</th>
                  <td><Link to={`/block/${tx.blockNumber}?chain=${chain}`}>{digits(tx.blockNumber)}</Link></td>
                </tr>
                <tr>
                  <th>瓦斯</th>
                  <td>{digits(tx.gas)}</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div className="col-sm-6">
            <table className="table table-striped">
              <tbody>
                <tr>
                  <th>瓦斯价格</th>
                  <td>{digits(tx.gasPrice)}</td>
                </tr>
                <tr>
                  <th>瓦斯使用</th>
                  <td>{digits(tx.gasUsed)}</td>
                </tr>
                <tr>
                  <th>累计瓦斯使用</th>
                  <td>{digits(tx.cumulativeGasUsed)}</td>
                </tr>
                <tr>
                  <th>随机数</th>
                  <td>{digits(tx.nonce)}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }

  renderTx(tx, chain, unit) {
    return (
      <div className="panel-txs">
        <div className="panel panel-default panel-tx">
          <div className="panel-heading">
            <div className="panel-title clearfix">
              <div className="pull-right tx-time">{moment(tx.timestamp*1000).format('YYYY-MM-DD HH:mm:ss')}</div>
              <div><Link to={`/tx/${tx.hash}?chain=${chain}`}><code>{tx.hash}</code></Link></div>
            </div>
          </div>
          <div className="panel-body">
            <div className="row tx-item">
              <div className="col-sm-5">
                <Link to={`/address/${tx.from}?chain=${chain}`}><code>{tx.from}</code></Link>
              </div>
              <div className="col-sm-2 text-center tx-arrow">
                <span className="lead" style={{ fontFamily: 'sans-serif' }}>&rarr;</span>
              </div>
              <div className="col-sm-5">
                <Link to={`/address/${tx.to}?chain=${chain}`}><code>{tx.to}</code></Link>
              </div>
            </div>
            <div className="text-right">
              <div className="lead" style={{ fontSize: 30, marginBottom: 0 }}>
                <Price value={tx.value} />
                <small className="text-muted">{unit}</small>
              </div>
              <div className="span-fee"><strong>手续费: </strong><Price value={tx.fee} /><em className="text-muted">{unit}</em></div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderSub({ token_txs, tx, chain, unit }) {
    return ({
      tkn: this.renderTknTx,
      eth: this.renderLogs
    })[this.state.selected]({ token_txs, tx, chain, unit })
  }

  renderTknTx({ token_txs, chain }) {
    return (
      <div className="panel-txs">
        {token_txs.map((tx, index) => (
          <div className="panel-txs" key={index}>
            <div className="panel panel-default panel-tx">
              <div className="panel-heading">
                <div className="panel-title clearfix">
                  <div>
                    <span className="label label-success" style={{ marginRight: 5 }}>{tx.type}</span>
                    <Link to={`/tx/${tx.hash}?chain=${chain}`}><code>{tx.hash}</code></Link>
                  </div>
                </div>
              </div>
              <div className="panel-body">
                <div className="row tx-item">
                  <div className="col-sm-5">
                    <Link to={`/address/${tx.from}?chain=${chain}`}><code>{tx.from}</code></Link>
                  </div>
                  <div className="col-sm-2 text-center tx-arrow">
                    <span className="lead" style={{ fontFamily: 'sans-serif' }}>&rarr;</span>
                  </div>
                  <div className="col-sm-5">
                    <Link to={`/address/${tx.to}?chain=${chain}`}><code>{tx.to}</code></Link>
                  </div>
                </div>
                <div className="text-right">
                  <div className="lead" style={{ fontSize: 30, marginBottom: 0 }}>
                    <Price value={tx.value} />
                    <small className="text-muted">{tx.token_name}</small>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    )
  }

  renderLogs({ tx, chain, unit }) {
    return (
      <div>
        {tx.logs.length ? (
          <div>
            {tx.logs.map((log, index) => (
              <table className="table table-striped table-logs" key={index}>
                <tbody>
                  <tr>
                    <th>address</th>
                    <td><Link to={`/address/${log.address}?chain=${chain}`}><span className="hashcode">{log.address}</span></Link></td>
                  </tr>
                  <tr>
                    <th>data</th>
                    <td><span className="hashcode">{log.data}</span></td>
                  </tr>
                  <tr>
                    <th>topics</th>
                    <td>
                      {log.topics.map((topic, index) => (
                        <div key={index}><span className="hashcode">{topic}</span></div>
                      ))}
                    </td>
                  </tr>
                </tbody>
              </table>
            ))}
          </div>
        ) : (
          <div className="alert alert-warning">没有日志</div>
        )}
      </div>
    )
  }
}

EthereumTx.propTypes = {
  data: PropTypes.shape({
    chain: PropTypes.string.isRequired,
    tx: PropTypes.object.isRequired
  }).isRequired
}
