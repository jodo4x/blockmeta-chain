import React, { PropTypes, Component } from 'react'
import Link from 'react-router/lib/Link'
import moment from 'moment'
import digits from '../../lib/digits'
import Price from '../Price'

import 'moment/locale/zh-cn'
moment.locale('zh')

export default class BitcoinTx extends Component {
  constructor() {
    super()
    ;['renderTx', 'renderScripts'].forEach((handler) => {
      this[handler] = this[handler].bind(this)
    })
  }

  render() {
    const {
      data: { chain, tx }
    } = this.props

    return (
      <div className="tx-container content">
        <div>
          <h2>概要</h2>

          <div className="row">
            <div className="col-sm-6">
              <table className="table table-striped">
                <tbody>
                  <tr>
                    <th>余额</th>
                    <td><Price value={tx.total_output} /> <em className="text-muted">BTC</em></td>
                  </tr>
                  <tr>
                    <th>手续费</th>
                    <td>{tx.fee} <em className="text-muted unit">BTC</em></td>
                  </tr>
                  <tr>
                    <th>交易时间</th>
                    <td>{moment(tx.tx_time*1000).fromNow()}</td>
                  </tr>
                  <tr>
                    <th>确认数</th>
                    <td className={tx.confirmation ? 'text-success' : ''}>
                      {tx.confirmation ? (
                        <span>
                          <i className="fa fa-check-circle" />
                          <span>{tx.confirmation}</span>
                        </span>
                      ) : (
                        <span>
                          <span className="text-warning"><i className="fa fa-circle-o-notch fa-spin" />等待确认...</span>
                        </span>
                      )}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="col-sm-6">
              <table className="table table-striped">
                <tbody>
                  <tr>
                    <th>所属区块</th>
                    <td><Link to={`/block/${tx.block_height}?chain=${chain}`}>{digits(tx.block_height)}</Link></td>
                  </tr>
                  <tr>
                    <th>大小</th>
                    <td>{digits(tx.size)} <em className="text-muted unit">Bytes</em></td>
                  </tr>
                  <tr>
                    <th>币销毁</th>
                    <td>{tx.coindd ? <Price value={tx.coindd} /> : '0'}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

        </div>

        {this.renderTx(tx, chain)}
        {this.renderScripts(tx)}
      </div>
    )
  }

  renderTx(tx, chain) {
    const { inputs, outputs } = tx

    return (
      <div className="panel-txs">
        <div className="panel panel-default panel-tx">
          <div className="panel-heading">
            <div className="panel-title clearfix">
              <div className="pull-right tx-time">{moment(tx.tx_time*1000).format('YYYY-MM-DD HH:mm:ss')}</div>
              <div>
                {tx.is_coinbase === 1 && (
                  <span
                    className="label label-success"
                    style={{ marginRight: 5 }}
                  >
                    <span>COINBASE</span>
                  </span>
                )}
                <Link to={`/tx/${tx.hash}?chain=${chain}`}><code>{tx.hash}</code></Link>
              </div>
            </div>
          </div>

          <div className="panel-body">

            <div className="row tx-item">
              <div className="col-sm-5 addr-list-container">
                {tx.is_coinbase === 1 ? (
                  <span>(没有输入)新生块奖励</span>
                ) : (
                  <ul className="addr-list list-unstyled addr-out">
                    {inputs.map((tx, index) => (
                      <li key={index}>
                        <span className="pull-right"><Price value={tx.amount} /><em className="text-muted">BTC</em></span>
                        {tx.addr !== 'Unknown' ? (
                          <Link to={`/address/${tx.addr}?chain=${chain}`}><code>{tx.addr}</code></Link>
                        ) : (
                          <span style={{ fontSize: 14 }}>未知</span>
                        )}
                      </li>
                    ))}
                  </ul>
                )}
              </div>
              <div className="col-sm-2 text-center tx-arrow">
                <span className="lead" style={{ fontFamily: 'sans-serif' }}>&rarr;</span>
              </div>
              <div className="col-sm-5 addr-list-container">
                <ul className="addr-list list-unstyled">
                  {outputs.map((tx, index) => (
                    <li key={index}>
                      <span className="pull-right"><Price value={tx.amount} /><em className="text-muted">BTC</em></span>
                      {tx.addr !== 'Unknown' ? (
                        <Link to={`/address/${tx.addr}?chain=${chain}`}><code>{tx.addr}</code></Link>
                      ) : (
                        <span style={{ fontSize: 14 }}>未知</span>
                      )}
                    </li>
                  ))}
                </ul>
              </div>
            </div>

            <div className="text-right">
              <div className="lead" style={{ fontSize: 30, marginBottom: 0 }}>
                <Price value={tx.total_output} />
                <small className="text-muted">BTC</small>
              </div>
              <div className="span-fee"><strong>手续费:</strong> {(!!(+tx.fees)) ? <Price value={tx.fees} /> : '0'} <em className="text-muted">BTC</em></div>
            </div>

          </div>
        </div>
      </div>
    )
  }

  renderScripts({ inputs, outputs }) {
    return (
      <div className="tx-scripts">
        <div>
          <h3>输入</h3>
          <table className="table table-striped">
            <tbody>
              {inputs.map((item, index) => (
                <tr key={index}>
                  <td><samp>{item.script}</samp></td>
                </tr>
              ))}
            </tbody>
          </table>

          <h3 style={{ marginTop: 25 }}>输出</h3>
          <table className="table table-striped">
            <tbody>
              {outputs.map((item, index) => (
                <tr key={index}>
                  <td><samp>{item.script}</samp></td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

BitcoinTx.propTypes = {
  data: PropTypes.shape({
    chain: PropTypes.string.isRequired,
    tx: PropTypes.shape({
      inputs: PropTypes.array.isRequired,
      outputs: PropTypes.array.isRequired
    }).isRequired
  }).isRequired
}
