import React, { PropTypes, Component } from 'react'

export default class SearchBar extends Component {
  constructor() {
    super()
    this.handleSearch = this.handleSearch.bind(this)
  }

  render() {
    return (
      <div className="searchbar well">
        <form className="form-inline" onSubmit={this.handleSearch}>
          <input className="form-control input-lg" type="text" ref="keyword" placeholder="e.g. 178.22.89.102" />
          <button className="btn btn-primary btn-lg" type="submit">search</button>
        </form>
      </div>
    )
  }

  handleSearch(e) {
    e.preventDefault()
    this.props.searchNodes('bitcoin', this.refs.keyword.value)
  }
}

SearchBar.propTypes = {
  searchNodes: PropTypes.func.isRequired
}
