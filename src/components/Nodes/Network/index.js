import React, { PropTypes, Component } from 'react'
import connect from 'react-redux/lib/components/connect'
import Pagination from 'react-bootstrap/lib/Pagination'
import countries from '../../../constants/countries'
import moment from 'moment'

import SearchBar from './SearchBar'

class Network extends Component {
  constructor() {
    super()
    this.handlePagination = this.handlePagination.bind(this)
  }

  componentDidMount() {
    this.props.loadNodes()
  }

  render() {
    const { nodes } = this.props.data
    const { items, no_page, pages } = nodes

    return items ? (
      <div className="network">
        <h2>Network</h2>
        <SearchBar searchNodes={this.props.searchNodes} />
        <table className="table table-striped">
          <thead>
            <tr>
              <th>ip</th>
              <th>port</th>
              <th>client version</th>
              <th>subversion</th>
              <th>time</th>
              <th>country</th>
            </tr>
          </thead>
          <tbody>
            {items.map((node, index) => (
              <tr key={index}>
                <td>{node.ip}</td>
                <td>{node.port}</td>
                <td>{node.version}</td>
                <td>{node.subver}</td>
                <td>{moment(node.ntime*1000).toNow()}</td>
                <td>
                  <span>
                    <img
                      src={`//cdnjs.cloudflare.com/ajax/libs/flag-icon-css/2.3.1/flags/4x3/${node['location'].toLowerCase()}.svg`}
                      alt={countries[node['location']] || "n/a"}
                      style={{ width: 20, height: 15 }}
                    />
                    <span>{countries[node['location']]}</span>
                  </span>
                </td>
              </tr>
            ))}
          </tbody>
        </table>

        <div className="pager-container">
          <Pagination
            prev
            next
            maxButtons={10}
            activePage={no_page}
            items={pages}
            onSelect={this.handlePagination}
          />
        </div>
      </div>
    ) : null
  }

  handlePagination(page) {
    this.props.loadNodes('bitcoin', page)
  }
}

Network.propTypes = {
  loadNodes: PropTypes.func.isRequired,
  searchNodes: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired
}

export default connect(
  state => ({
    data: {
      nodes: state.nodes.nodes
    }
  })
)(Network)
