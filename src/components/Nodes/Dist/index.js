import React, { PropTypes } from 'react'
import countries from '../../../constants/countries'

export default function Dist({ data: { total, locals } }) {
  return (
    <div className="dist">
      <table className="table">
        <tbody>
          {Object.keys(locals).slice(0, 10).map((key, index) => (
            <tr key={index}>
              <td>{++index}</td>
              <td>
                <span className="flag">
                  <img
                    src={`//cdnjs.cloudflare.com/ajax/libs/flag-icon-css/2.3.1/flags/4x3/${key.toLowerCase()}.svg`}
                    alt={countries[key] || "n/a"}
                    style={{ width: 20, height: 15 }}
                  />
                </span>
                <span className="name">{countries[key]}</span>
              </td>
              <td>{locals[key]}<sup className="text-muted">{(locals[key]/total*100).toFixed(2)}%</sup></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

Dist.propTypes = {
  data: PropTypes.shape({
    total: PropTypes.number.isRequired,
    locals: PropTypes.object.isRequired
  }).isRequired
}
