import React, { PropTypes, Component } from 'react'
import bindActionCreators from 'redux/lib/bindActionCreators'
import connect from 'react-redux/lib/components/connect'
import { loadDist, loadNodes, searchNodes } from '../../actions/nodes'

import DistMap from './DistMap'
import Dist from './Dist'
import Network from './Network'
import Subv from './Subv'

if (process.env.BROWSER) {
  require('./index.scss')
}

class Nodes extends Component {
  static fetchData({ store }) {
    return Promise.all([
      store.dispatch(loadDist()),
      store.dispatch(loadNodes())
    ])
  }

  componentDidMount() {
    this.props.loadDist()
  }

  render() {
    const { dist, nodes } = this.props
    const { total, locals, subvers } = dist

    return (
      <div className="container">
        <div className="nodes">
          <div className="dist-container">
            <h2>Distribution</h2>
            <div className="row">
              <div className="col-sm-8">
                {total && locals ?
                  <DistMap data={{ total, locals }} /> :
                  <span>...</span>}
              </div>
              <div className="col-sm-4">
                {total && locals ?
                  <Dist data={{ total, locals }} /> :
                  <span>...</span>}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-8">
              <Network
                data={{ nodes }}
                loadNodes={this.props.loadNodes}
                searchNodes={this.props.searchNodes}
              />
            </div>
            <div className="col-sm-4">
              {subvers ?
                <Subv data={{ subvers }} /> :
                <span>...</span>}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Nodes.propTypes = {
  loadDist: PropTypes.func.isRequired,
  loadNodes: PropTypes.func.isRequired,
  searchNodes: PropTypes.func.isRequired,
  dist: PropTypes.object.isRequired,
  nodes: PropTypes.object.isRequired
}

export default connect(
  state => ({
    dist: state.nodes.dist,
    nodes: state.nodes.nodes
  }),

  dispatch => bindActionCreators({
    loadDist,
    loadNodes,
    searchNodes
  }, dispatch)
)(Nodes)
