import React, { PropTypes } from 'react'

export default function Subv({ data: { subvers } }) {
  return (
    <div className="subv">
      <h2>Subversions</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>version</th>
            <th>number</th>
          </tr>
        </thead>
        <tbody>
          {Object.keys(subvers).map((key, index) => (
            <tr key={index}>
              <td><strong>{key}</strong></td>
              <td>{subvers[key]}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

Subv.propTypes = {
  data: PropTypes.shape({
    subvers: PropTypes.object.isRequired
  }).isRequired
}
