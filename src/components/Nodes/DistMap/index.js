import React, { PropTypes, Component } from 'react'
import Highmaps from 'highcharts/highmaps'
import world from '../../../constants/mapdata/world'
import countries from '../../../constants/countries'

export default class DistMap extends Component {
  componentDidMount() {
    const { total, locals } = this.props.data
    const mapData = Highmaps.geojson(world)
    const data = []

    for (let p in locals) {
      data.push({ code: p, z: locals[p], name: countries[p] })
    }

    this.map = new Highmaps.Map(this.refs.map, {
      title: {
        text: null
      },
      subtitle: {
        text: 'Map shows concentration of reachable Bitcoin nodes found in countries around the world',
        style: {
          color: '#bdc3c7'
        }
      },
      legend: {
        enabled: false
      },
      series: [{
        type: 'map',
        nullColor: '#d2d7d3',
        borderColor: '#bdc3c7',
        mapData: mapData,
        enableMouseTracking: false
      }, {
        type: 'mapbubble',
        color: '#6c7a89',
        name: 'Node distribution',
        mapData: mapData,
        joinBy: ['iso-a2', 'code'],
        data: data
      }],
      tooltip: {
        backgroundColor: '#000000',
        borderWidth: 0,
        shadow: false,
        useHTML: true,
        formatter: function() {
          return `
            <span style='display: block;'>${this.point.name}</span>
            <span style='display: block;'>
              <span style='display: inline-block; height: 32px; margin-right: 5px;'>
                <span class='f32'><span class='flag ${this.point.code.toLowerCase()}'></span></span>
              </span>
              <span style='font-size: 18px; letter-spacing: -2px; position: relative; top: 7px;'>
                ${this.point.z}
                <sub class="text-muted">${(this.point.z/total*100).toFixed(2)}%</sub>
              </span>
            </span>
          `
        },
        pointFormat: '<span>{point.name}</span> <strong>{point.z}</strong> nodes',
        style: {
          color: '#fff'
        }
      },
      mapNavigation: {
        enabled: false,
        buttonOptions: {
          verticalAlign: 'bottom',
          theme: {
            fill: '#ffffff',
            r: 2,
            states: {
              hover: {
                fill: '#f1f1f1',
                stroke: '#bdc3c7'
              }
            }
          }
        }
      },
      credits: {
        enabled: false
      }
    })
  }

  componentWillUnmount() {
    this.map.destroy()
  }

  render() {
    return (
      <div className="dist-map">
        <div style={{ width: '100%', height: 380 }} ref="map">Loading...</div>
      </div>
    )
  }
}

DistMap.propTypes = {
  data: PropTypes.shape({
    total: PropTypes.number.isRequired,
    locals: PropTypes.object.isRequired
  }).isRequired
}
