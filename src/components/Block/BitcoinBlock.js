import React, { PropTypes, Component } from 'react'
import moment from 'moment'
import Link from 'react-router/lib/Link'
import Pagination from 'react-bootstrap/lib/Pagination'
import Price from '../Price'
import TxSwitcher from '../TxSwitcher'
import digits from '../../lib/digits'

import 'moment/locale/zh-cn'
moment.locale('zh')

export default class BitcoinBlock extends Component {
  constructor() {
    super()

    ;[
      'renderSumm',
      'renderTx',
      'renderTxHorizontal',
      'renderTxVertical'
    ].forEach((handler) => {
      this[handler] = this[handler].bind(this)
    })
  }

  render() {
    const {
      data: { block, chain, txSwitchType },
      handlePagination
    } = this.props


    return (
      <div className="content">

        {this.renderSumm(block, chain)}

        <div className="panel-txs">
            {block.tx.length ? (
              <div>

                {block.tx.map((tx, index) => (
                  <div className="panel panel-default panel-tx" key={index}>
                    <div className="panel-heading">
                      <div className="panel-title clearfix">
                        <div className="pull-right tx-time">{moment(tx.tx_time*1000).format('YYYY-MM-DD HH:mm:ss')}</div>
                        <div>
                          {tx.is_coinbase === 1 && (
                            <span className="label label-success label-coinbase">
                              <span>COINBASE</span>
                            </span>
                          )}
                          <Link to={`/tx/${tx.hash}?chain=${chain}`}><code>{tx.hash}</code></Link>
                        </div>
                      </div>
                    </div>
                    <div className="panel-body" style={{ position: 'relative' }}>
                      {this.renderTx(tx, chain, txSwitchType)}
                    </div>
                  </div>
                ))}

                <div className="pager-container">
                  <div>总共 {block.pages} 页 {block.tx_num} 个交易</div>
                  <Pagination
                    prev
                    next
                    maxButtons={10}
                    activePage={block.no_page}
                    items={block.pages}
                    onSelect={handlePagination}
                  />
                </div>

              </div>
            ) : (
              <div className="alert alert-warning">没有交易.</div>
            )}
        </div>

      </div>
    )
  }

  renderSumm(block, chain) {
    return (
      <div>
        <h2>概要</h2>
        <div className="row">
          <div className="col-sm-6">
            <table className="table table-striped table-summ">
              <tbody>
                <tr>
                  <th>矿池</th>
                  <td>
                    {block.relay && <i className={`icon-pool icon-pool-${block.relay.replace(/\./g, '').toLowerCase()}`} />}
                    {block.relay !== 'Unknown' ? (
                      <Link to={`/miner/${block.relay}`}>
                        {block.relay}
                      </Link>
                    ) : (
                      <span>未知</span>
                    )}
                  </td>
                </tr>
                <tr>
                  <th>时间</th>
                  <td>{moment(block.time*1000).fromNow()}</td>
                </tr>
                <tr>
                  <th>总转出量</th>
                  <td><Price value={block.values} /><em className="text-muted">BTC</em></td>
                </tr>
                <tr>
                  <th>奖励</th>
                  <td>{block.reward} <em className="text-muted">BTC</em></td>
                </tr>
                <tr>
                  <th>交易数</th>
                  <td>{block.tx_num}</td>
                </tr>
                <tr>
                  <th>块大小</th>
                  <td>{block.size} <em className="text-muted">KB</em></td>
                </tr>
                <tr>
                  <th>难度</th>
                  <td>{block.difficulty && digits(block.difficulty.replace(/\s/g, ''))}</td>
                </tr>
                <tr>
                  <th>随机数</th>
                  <td>{block.nonuce}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="col-sm-6">
            <table className="table table-striped table-summ">
              <tbody>
                <tr>
                  <th>币天销毁</th>
                  <td><Price value={block.coin_dd} /></td>
                </tr>
                <tr>
                  <th>区块哈希</th>
                  <td><span className="hashcode" title={block.hash}>{block.hash}</span></td>
                </tr>
                <tr>
                  <th>上一块</th>
                  <td><Link to={`/block/${block.prev_block_hash}?chain=${chain}`}><span className="hashcode" title={block.prev_block_hash}>{block.prev_block_hash}</span></Link></td>
                </tr>
                <tr>
                  <th>下一块</th>
                  <td><Link to={`/block/${block.next_hash}?chain=${chain}`}><span className="hashcode" title={block.next_hash}>{block.next_hash}</span></Link></td>
                </tr>
                <tr>
                  <th>梅克尔树</th>
                  <td><span className="hashcode" title={block.merkle_root}>{block.merkle_root}</span></td>
                </tr>
                <tr>
                  <th>版本</th>
                  <td>{block.version}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }

  renderTx(tx, chain, txSwitchType) {
    return ({
      h: this.renderTxHorizontal,
      v: this.renderTxVertical
    })[txSwitchType](tx, chain)
  }

  renderTxHorizontal(tx, chain) {
    return (
      <div>
        <div className="row tx-item">
          <div className="col-sm-5 addr-list-container">
            {tx.is_coinbase === 1 ? (
              <span>没有输入(新生块奖励)</span>
            ) : (
              <ul className="addr-list list-unstyled addr-out">
                {tx.tx_in.map((tx, index) => (
                  <li key={index}>
                    <span className="pull-right"><Price value={tx.amount} /><em className="text-muted">BTC</em></span>
                    <Link to={`/address/${tx.addr}?chain=${chain}`}><code>{tx.addr}</code></Link>
                  </li>
                ))}
              </ul>
            )}
          </div>
          <div className="col-sm-2 text-center tx-arrow">
            <span className="lead" style={{ fontFamily: 'sans-serif' }}>&rarr;</span>
          </div>
          <div className="col-sm-5 addr-list-container">
            <ul className="addr-list list-unstyled">
              {tx.tx_out.map((tx, index) => (
                <li key={index}>
                  <span className="pull-right"><Price value={tx.amount} /><em className="text-muted">BTC</em></span>
                  <Link to={`/address/${tx.addr}?chain=${chain}`}><code>{tx.addr}</code></Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
        <div className="text-right">
          <div className="lead" style={{ fontSize: 30, marginBottom: 0 }}>
            <Price value={tx.total_amount} />
            <small className="text-muted">BTC</small>
          </div>
          <div className="span-fee"><strong>手续费:</strong><Price value={tx.fees} /><em className="text-muted">BTC</em></div>
        </div>
      </div>
    )
  }

  renderTxVertical(tx, chain) {
    return (
      <div className="row">
        <div className="col-sm-7">
          <div className="addr-list-container">
            <dl className="addr-list addr-list-v">
              <dt>
                <ul className="list-unstyled">
                  {tx.tx_in.map((tx, index) => (
                    <li key={index}>
                      <span className="pull-right"><Price value={tx.amount} /><em className="text-muted">BTC</em></span>
                      <Link to={`/address/${tx.addr}?chain=${chain}`}><code>{tx.addr}</code></Link>
                    </li>
                  ))}
                </ul>
              </dt>
              <dd style={{ paddingLeft: 10, position: 'relative'  }}>
                <ul className="list-unstyled">
                  {tx.tx_out.map((tx, index) => (
                    <li key={index}>
                      <span className="pull-right"><Price value={tx.amount} /><em className="text-muted">BTC</em></span>
                      <Link to={`/address/${tx.addr}?chain=${chain}`}><code>{tx.addr}</code></Link>
                    </li>
                  ))}
                </ul>
              </dd>
            </dl>
          </div>
        </div>
        <div className="col-sm-5 text-center">
          <div className="amount">
            <div className="lead" style={{ fontSize: 30, marginBottom: 0 }}>
              <Price value={tx.total_amount} />
              <small className="text-muted">BTC</small>
            </div>
            <div className="text-muted">(交易额)</div>
          </div>
          <div>花费 <Price value={tx.fees} /><em className="text-muted">BTC</em></div>
        </div>
      </div>
    )
  }
}

BitcoinBlock.propTypes = {
  handlePagination: PropTypes.func.isRequired,
  data: PropTypes.shape({
    chain: PropTypes.string.isRequired,
    block: PropTypes.shape({
      tx: PropTypes.array.isRequired,
      no_page: PropTypes.number.isRequired,
      pages: PropTypes.number.isRequired
    }).isRequired,
    txSwitchType: PropTypes.string.isRequired
  }).isRequired
}
