import React, { PropTypes, Component } from 'react'
import moment from 'moment'
import Link from 'react-router/lib/Link'
import Pagination from 'react-bootstrap/lib/Pagination'
import Price from '../Price'
import TxSwitcher from '../TxSwitcher'
import digits from '../../lib/digits'

import 'moment/locale/zh-cn'
moment.locale('zh')

export default class EthereumBlock extends Component {
  constructor(props) {
    super()

    this.state = {
      unit: props.data.chain === 'ethereum' ? 'ETH' : 'ETC'
    }

    ;[
      'renderSumm',
      'renderTx',
      'renderTxHorizontal',
      'renderTxVertical'
    ].forEach((handler) => {
      this[handler] = this[handler].bind(this)
    })
  }

  render() {
    const { unit } = this.state
    const {
      data: { block, chain, txSwitchType },
      handlePagination
    } = this.props

    return (
      <div className="content">

        {this.renderSumm(block, chain, unit)}

        <div className="panel-txs">
          {block.transactions.length ? (
            <div>
              {block.transactions.map((tx, index) => (
                <div className="panel panel-default panel-tx" key={index}>
                  <div className="panel-heading">
                    <div className="panel-title clearfix">
                      <div className="pull-right tx-time">{moment(tx.timestamp*1000).format('YYYY-MM-DD HH:mm:ss')}</div>
                      <div><Link to={`/tx/${tx.hash}?chain=${chain}`}><code>{tx.hash}</code></Link></div>
                    </div>
                  </div>
                  <div className="panel-body">
                    {this.renderTx(tx, chain, txSwitchType, unit)}
                  </div>
                </div>
              ))}
              <div className="pager-container">
                <div>总共 {block.pages} 页 {block.txs_num} 个交易</div>
                <Pagination
                  prev
                  next
                  maxButtons={10}
                  activePage={block.no_page}
                  items={block.pages}
                  onSelect={handlePagination}
                />
              </div>
            </div>
          ) : (
            <div className="alert alert-warning">没有交易.</div>
          )}
        </div>

      </div>
    )
  }

  renderSumm(block, chain, unit) {
    return (
      <div>
        <h2>概览</h2>
        <div className="row">
          <div className="col-sm-6">
            <table className="table table-striped table-summ">
              <tbody>
                <tr>
                  <th>时间</th>
                  <td>{moment(block.timestamp*1000).fromNow()}</td>
                </tr>
                <tr>
                  <th>奖励</th>
                  <td><Price value={block.reward} /><em className="text-muted">{unit}</em></td>
                </tr>
                <tr>
                  <th>交易数</th>
                  <td>{digits(block.txs_num)}</td>
                </tr>
                <tr>
                  <th>块大小</th>
                  <td>{digits(block.size)}</td>
                </tr>
                <tr>
                  <th>瓦斯限值</th>
                  <td>{digits(block.gasLimit)}</td>
                </tr>
                <tr>
                  <th>消耗瓦斯</th>
                  <td>{digits(block.gasUsed)}</td>
                </tr>
                <tr>
                  <th>随机数</th>
                  <td><span>{block.nonce}</span></td>
                </tr>
                <tr>
                  <th>确认数</th>
                  <td>{digits(block.confirmation)}</td>
                </tr>
                <tr>
                  <th>难度</th>
                  <td>{block.difficulty}</td>
                </tr>
                <tr>
                  <th>总难度</th>
                  <td>{block.totalDifficulty}</td>
                </tr>
                <tr>
                  <th>矿工</th>
                  <td>
                    <span className="hashcode" style={{ width: '80%' }}>{block.miner}</span>
                    <sup className="miner-tag" title="矿工标签">{block.miner_tag}</sup>
                  </td>
                </tr>
                <tr>
                  <th>叔区块数</th>
                  <td>{block.num_uncle}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="col-sm-6">
            <table className="table table-striped table-summ">
              <tbody>
                <tr>
                  <th>区块哈希</th>
                  <td><span className="hashcode">{block.hash}</span></td>
                </tr>
                <tr>
                  <th>上一块</th>
                  <td><Link to={`/block/${block.last}?chain=${chain}`}><span className="hashcode">{block.last}</span></Link></td>
                </tr>
                <tr>
                  <th>下一块</th>
                  <td><Link to={`/block/${block.next}?chain=${chain}`}><span className="hashcode">{block.next}</span></Link></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }

  renderTx(tx, chain, txSwitchType, unit) {
    return ({
      h: this.renderTxHorizontal,
      v: this.renderTxVertical
    })[txSwitchType](tx, chain, unit)
  }

  renderTxHorizontal(tx, chain, unit) {
    return (
      <div>
        <div className="row tx-item">
          <div className="col-sm-5">
            <Link to={`/address/${tx.from}?chain=${chain}`}><code>{tx.from}</code></Link>
          </div>
          <div className="col-sm-2 text-center tx-arrow">
            <span className="lead" style={{ fontFamily: 'sans-serif' }}>&rarr;</span>
          </div>
          <div className="col-sm-5">
            <Link to={`/address/${tx.to}?chain=${chain}`}><code>{tx.to}</code></Link>
          </div>
        </div>
        <div className="text-right">
          <div className="lead" style={{ fontSize: 30, marginBottom: 0 }}>
            <Price value={tx.value} />
            <small className="text-muted">{unit}</small>
          </div>
          <div className="span-fee"><strong>手续费: </strong><Price value={tx.fee} /><em className="text-muted">{unit}</em></div>
        </div>
      </div>
    )
  }

  renderTxVertical(tx, chain, unit) {
    return (
      <div className="row">
        <div className="col-sm-7">
          <div className="addr-list-container">
            <dl className="addr-list addr-list-v">
              <dt>
                <ul className="list-unstyled">
                  <li><Link to={`/address/${tx.from}?chain=${chain}`}><code>{tx.from}</code></Link></li>
                </ul>
              </dt>
              <dd style={{ paddingLeft: 10, position: 'relative'  }}>
                <ul className="list-unstyled">
                  <li><Link to={`/address/${tx.to}?chain=${chain}`}><code>{tx.to}</code></Link></li>
                </ul>
              </dd>
            </dl>
          </div>
        </div>
        <div className="col-sm-5 text-center">
          <div className="amount">
            <div className="lead" style={{ fontSize: 30, marginBottom: 0 }}>
              <Price value={tx.value} />
              <small className="text-muted">{unit}</small>
            </div>
            <div className="text-muted">(总额度)</div>
          </div>
          <div>手续费: <Price value={tx.fee} /><em className="text-muted">{unit}</em></div>
        </div>
      </div>
    )
  }
}

EthereumBlock.propTypes = {
  handlePagination: PropTypes.func.isRequired,
  data: PropTypes.shape({
    chain: PropTypes.string.isRequired,
    block: PropTypes.shape({
      tx: PropTypes.array.isRequired,
      no_page: PropTypes.number.isRequired,
      pages: PropTypes.number.isRequired
    }).isRequired,
    txSwitchType: PropTypes.string.isRequired
  }).isRequired
}
