import React, { PropTypes, Component } from 'react'
import bindActionCreators from 'redux/lib/bindActionCreators'
import connect from 'react-redux/lib/components/connect'
import Pagination from 'react-bootstrap/lib/Pagination'
import Link from 'react-router/lib/Link'
import digits from '../../lib/digits'

import loadBlock from '../../actions/block'
import chainPattern from '../../constants/chainPattern'

import BitcoinBlock from './BitcoinBlock'
import EthereumBlock from './EthereumBlock'

if (process.env.BROWSER) {
  require('./index.scss')
}

class Block extends Component {
  static fetchData({ store, location, params }) {
    return store.dispatch(loadBlock(location.query.chain, params.id))
  }

  constructor() {
    super()
    this.state = {
      hasCorrectChainName: false
    }
    this.renderChild = this.renderChild.bind(this)
    this.handlePagination = this.handlePagination.bind(this)
  }

  componentWillMount() {
    if (chainPattern.test(this.props.location.query.chain)) {
      this.setState({
        hasCorrectChainName: true
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    const { loadBlock, params, location: { query } } = nextProps
    if (params.id !== this.props.params.id) {
      loadBlock(query.chain, params.id)
    }
  }

  componentDidMount() {
    const {
      'location': { query },
      params,
      loadBlock
    } = this.props

    loadBlock(query.chain, params.id)
  }

  render() {
    const { hasCorrectChainName } = this.state
    const {
      'location': { query },
      params,
      block
    } = this.props

    const height = query.chain === 'bitcoin' ? block.height : block.number

    return hasCorrectChainName ? (
      <div className="container">
        <div className="block">
          <ul className="breadcrumb">
            <li><Link to="/">首页</Link></li>
            <li><span>区块</span></li>
            <li className="active"><span>{height}</span></li>
          </ul>
          <div className="page-header">
            <h1>
              <span>{height}</span>
            </h1>
            <div className="text-muted"><strong>区块哈希:</strong> <span className="text-muted">{block.hash}</span></div>
          </div>

          {/* render child */}
          {this.renderChild(query.chain)}

        </div>
      </div>
    ) : null
  }

  renderChild(chain) {
    const { location, txSwitchType, block } = this.props

    let child
    switch(chain) {
      case 'bitcoin':
        return child = (
          <BitcoinBlock
            data={{ block, txSwitchType, chain: location.query.chain }}
            handlePagination={this.handlePagination}
          />
        )
      case 'ethereum':
      case 'ethereumclassic':
        return child = (
          <EthereumBlock
            data={{ block, txSwitchType, chain: location.query.chain }}
            handlePagination={this.handlePagination}
          />
        )
      default:
        return child
    }
  }

  handlePagination(page) {
    const { location, params, loadBlock } = this.props
    loadBlock(location.query.chain, params.id, page)
  }
}

Block.propTypes = {
  loadBlock: PropTypes.func.isRequired,
  block: PropTypes.shape({
    tx: PropTypes.array.isRequired,
    no_page: PropTypes.number.isRequired,
    pages: PropTypes.number.isRequired
  }).isRequired,
  txSwitchType: PropTypes.string.isRequired
}

export default connect(
    state => ({
      block: state.block,
      txSwitchType: state.ui.txSwitchType
    }),

    dispatch => bindActionCreators({ loadBlock }, dispatch)
)(Block)
