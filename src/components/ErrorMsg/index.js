import React, { PropTypes, Component } from 'react'
import connect from 'react-redux/lib/components/connect'
import { dismissError } from '../../actions/errorMsg'

if (process.env.BROWSER) {
  require('./index.scss')
}

class ErrorMsg extends Component {
  render() {
    const {
      'error': {
        status,
        message
      },
      dispatch
    } = this.props

    return message ? (
      <div className="error-msg">
        <div className={`alert alert-${status}`}>
          <button
            className="close"
            type="button"
            onClick={() => dispatch(dismissError())}
          >
            <span>&times;</span>
          </button>
          {message}
        </div>
      </div>
    ) : null
  }
}

ErrorMsg.propTypes = {
  'error': PropTypes.shape({
    'status': PropTypes.string.isRequired,
    // message: PropTypes.oneOf(['object', 'string']) // FIXME
  }).isRequired
}

export default connect(
  state => ({
    'error': state.errorMsg.error
  })
)(ErrorMsg)
