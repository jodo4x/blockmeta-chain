import React, { PropTypes } from 'react'
import Link from 'react-router/lib/Link'

if (process.env.BROWSER) {
  require('./index.scss')
}

export default function Footer({ data: { displayStyle }, toggleDisplayStyle }) {
  return (
    <div className="footer text-center">
      <div className="container">
        <div className="hidden">
          <span>显示:</span>
          {displayStyle === 'normal' ? (
            <span><strong>正常</strong></span>
          ) : (
            <button className="btn btn-link" type="button" onClick={() => toggleDisplayStyle('normal')}>正常</button>
          )}

          {displayStyle === 'wide' ? (
            <span><strong>宽</strong></span>
          ) : (
            <button className="btn btn-link" type="button" onClick={() => toggleDisplayStyle('wide')}>宽</button>
          )}
        </div>
        <div className="sub-links">
          <Link to="/about">关于</Link>
          <span className="text-muted">-</span>
          <Link to="/logs">日志</Link>
        </div>
        <div>&copy; 2016 blockmeta</div>
      </div>
    </div>
  )
}

Footer.propTypes = {
  toggleDisplayStyle: PropTypes.func.isRequired,
  data: PropTypes.shape({
    displayStyle: PropTypes.string.isRequired
  }).isRequired
}
