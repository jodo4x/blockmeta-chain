import React, { PropTypes } from 'react'
import Link from 'react-router/lib/Link'
import Price from '../Price'
import TxSwitcher from '../TxSwitcher'
import digits from '../../lib/digits'

export default function TxList({ data: { chain, tx, txSwitchType, tokenName } }) {
  return (
    <div className="panel panel-default panel-txs">
      <div className="hidden"><TxSwitcher /></div>
      <div className="panel-body">
        <div>
          {tx.map((txItem, index) => (
            <div className="panel panel-default panel-tx" key={index}>
              <div className="panel-heading">
                <div className="panel-title clearfix">
                  <div><span className="label label-success label-sm">{txItem.type}</span><Link to={`/tx/${txItem.hash}?chain=${chain}`}><code>{txItem.hash}</code></Link></div>
                </div>
              </div>
              <div className="panel-body">
                {txSwitchType === 'h' && <TxHorizontal data={{ chain, tx: txItem, tokenName }} />}
                {txSwitchType === 'v' && <TxVertical data={{ chain, tx: txItem, tokenName }} />}
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

TxList.propTypes = {
  data: PropTypes.shape({
    chain: PropTypes.string.isRequired,
    tx: PropTypes.array.isRequired,
    txSwitchType: PropTypes.string.isRequired
  }).isRequired
}

function TxHorizontal({ data: { chain, tx, tokenName } }) {
  return (
    <div>
      <div className="row tx-item">
        <div className="col-sm-5">
          <Link to={`/address/${tx.from}?chain=${chain}`}><code>{tx.from}</code></Link>
        </div>
        <div className="col-sm-2 text-center tx-arrow">
          <span className="lead" style={{ fontFamily: 'sans-serif' }}>&rarr;</span>
        </div>
        <div className="col-sm-5">
          <Link to={`/address/${tx.to}?chain=${chain}`}><code>{tx.to}</code></Link>
        </div>
      </div>
      <hr className="hidden" />
      <div className="row">
        <div className="col-sm-6">
          <table className="table table-striped table-txinfo hidden">
            <tbody>
              <tr>
                <th>类型</th>
                <td><span className="label label-info text-uppercase">{tx.type}</span></td>
              </tr>
              <tr>
                <th>区块</th>
                <td><Link to={`/block/${tx.block}`}>{digits(tx.block)}</Link></td>
              </tr>
              <tr>
                <th>badge</th>
                <td>{tx.badge}</td>
              </tr>
              <tr>
                <th>logIndex</th>
                <td><code className="hashcode">{tx.logIndex}</code></td>
              </tr>
              <tr>
                <th>交易哈希</th>
                <td><code className="hashcode">{tx.transactionHash}</code></td>
              </tr>
              <tr>
                <th>区块哈希</th>
                <td><code className="hashcode">{tx.blockHash}</code></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="col-sm-6">
          <div className="text-right">
            <div className="lead" style={{ fontSize: 30, marginBottom: 0 }}>
              <Price value={tx.value} />
              <small className="text-muted">{tokenName}</small>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

function TxVertical({ data: { chain, tx, tokenName } }) {
  return (
    <div className="row">
      <div className="col-sm-7">
        <div className="addr-list-container">
          <dl className="addr-list addr-list-v">
            <dt>
              <ul className="list-unstyled">
                <li><Link to={`/address/${tx.from}?chain=${chain}`}><code>{tx.from}</code></Link></li>
              </ul>
            </dt>
            <dd style={{ paddingLeft: 10, position: 'relative'  }}>
              <ul className="list-unstyled">
                <li><Link to={`/address/${tx.to}?chain=${chain}`}><code>{tx.to}</code></Link></li>
              </ul>
            </dd>
          </dl>
        </div>
      </div>
      <div className="col-sm-5 text-center">
        <div className="amount">
          <div className="lead" style={{ fontSize: 30, marginBottom: 0 }}>
            <Price value={tx.value} />
            <small className="text-muted">{tokenName}</small>
          </div>
          <div className="text-muted">(总余额)</div>
        </div>
        <table className="table table-striped table-txinfo text-left hidden">
          <tbody>
            <tr>
              <th>类型</th>
              <td><span className="label label-info text-uppercase">{tx.type}</span></td>
            </tr>
            <tr>
              <th>区块</th>
              <td><Link to={`/block/${tx.block}`}>{digits(tx.block)}</Link></td>
            </tr>
            <tr>
              <th>badge</th>
              <td>{tx.badge}</td>
            </tr>
            <tr>
              <th>logIndex</th>
              <td><code className="hashcode">{tx.logIndex}</code></td>
            </tr>
            <tr>
              <th>交易哈希</th>
              <td><code className="hashcode">{tx.transactionHash}</code></td>
            </tr>
            <tr>
              <th>区块哈希</th>
              <td><code className="hashcode">{tx.blockHash}</code></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  )
}
