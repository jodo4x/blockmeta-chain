import React, { PropTypes } from 'react'
import Link from 'react-router/lib/Link'
import digits from '../../lib/digits'

export default function HolderList({ data: { chain, holder, supply, tokenName } }) {
  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>排列</th>
            <th>地址</th>
            <th>余额<span className="text-muted" style={{ fontWeight: 400 }}>({tokenName})</span></th>
            <th>所占比</th>
          </tr>
        </thead>
        <tbody>
          {holder.map((h, index) => (
            <tr key={index}>
              <td>{index+1}</td>
              <td>
                <Link to={`/address/${h.account}?chain=${chain}`}>
                  <code>{h.account}</code>
                </Link>
              </td>
              <td>
                <span>
                  <span>{digits(h.balance)}</span>
                </span>
              </td>
              <td>
                {`${(h.balance/supply).toFixed(6)}%`}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

HolderList.propTypes = {
  data: PropTypes.shape({
    chain: PropTypes.string.isRequired,
    holder: PropTypes.array.isRequired,
    supply: PropTypes.string.isRequired,
    tokenName: PropTypes.string.isRequired
  }).isRequired
}
