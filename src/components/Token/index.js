import React, { PropTypes, Component } from 'react'
import bindActionCreators from 'redux/lib/bindActionCreators'
import connect from 'react-redux/lib/components/connect'
import loadToken from '../../actions/token'
import Link from 'react-router/lib/Link'
import Pagination from 'react-bootstrap/lib/Pagination'
import Price from '../Price'
import digits from '../../lib/digits'
import chainPattern from '../../constants/chainPattern'

import HolderList from './HolderList'
import TxList from './TxList'
import TokenLogo from './TokenLogo'

if (process.env.BROWSER) {
  require('./index.scss')
}

class Token extends Component {
  static fetchData({ store, location, params }) {
    return store.dispatch(loadToken(location.query.chain, params.token))
  }

  constructor(props) {
    super()

    this.state = {
      hasCorrectChainName: false,
      tokenName: props.params.token,
      activeKey: 'tx',
      filters: {
        tx: '交易',
        holder: '账户'
      },
      sites: {
        DGD: 'https://www.dgx.io/dgd',
        DCS: 'http://8btc.com'
      }
    }

    ;['renderChild', 'handleSelect', 'handlePagination'].forEach((handler) => {
      this[handler] = this[handler].bind(this)
    })
  }

  componentWillMount() {
    if (chainPattern.test(this.props.location.query.chain)) {
      this.setState({
        hasCorrectChainName: true
      })
    }
  }

  componentDidMount() {
    const { location, params, loadToken } = this.props
    loadToken(location.query.chain, params.token)
  }

  render() {
    const { hasCorrectChainName, activeKey, filters, tokenName, sites } = this.state
    const {
      'location': { query: { chain } },
      params,
      token
    } = this.props

    return hasCorrectChainName ? (
      <div className="container">
        <div className="token">

          <ul className="breadcrumb">
            <li><Link to="/">首页</Link></li>
            <li className="active"><span>{params.token}</span></li>
          </ul>

          <div className="row">
            <div className="col-sm-6">
              <div style={{ textAlign: 'center', paddingTop: 50 }}>
                <TokenLogo tokenName={tokenName} />
              </div>
            </div>
            <div className="col-sm-6">
              <div className="text-center" style={{ marginBottom: 20 }}>
                <h1>{params.token}</h1>
              </div>
              <table className="table table-striped">
                <tbody>
                  <tr>
                    <th>合约地址</th>
                    <td><Link to={`/address/${token.contract_addr}?chain=${chain}`}><code>{token.contract_addr}</code></Link></td>
                  </tr>
                  <tr>
                    <th>供应量</th>
                    <td><span>{digits(token.supply)}</span></td>
                  </tr>
                  <tr>
                    <th>账户数</th>
                    <td><span>{digits(token.holders_num)}</span></td>
                  </tr>
                  <tr>
                    <th>交易量</th>
                    <td><span>{digits(token.txs_num)}</span></td>
                  </tr>
                  <tr>
                    <th>官方网站</th>
                    <td><a href={sites[tokenName]} target="_blank">{sites[tokenName]}</a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <div>
            <ul className="nav nav-tabs">
              {Object.keys(filters).map((key, index) => (
                <li className={activeKey === key ? 'active' : ''} key={index}>
                  <a href onClick={(e) => this.handleSelect(e, key)}>
                    <div>{filters[key]}</div>
                  </a>
                </li>
              ))}
            </ul>

            <div className="content">
              {this.renderChild(chain)}
              <div className="pager-container">
                <Pagination
                  prev
                  next
                  maxButtons={10}
                  activePage={token.no_page}
                  items={token.pages}
                  onSelect={this.handlePagination}
                />
              </div>
            </div>

          </div>
        </div>
      </div>
    ) : null
  }

  renderChild(chain) {
    const { activeKey, tokenName } = this.state
    const { token: { holder, tx, supply }, txSwitchType } = this.props

    let child
    switch(activeKey) {
      case 'tx':
        return child = (
          <TxList
            data={{ chain, tx, txSwitchType, tokenName }}
          />
        )
      case 'holder':
        return child = (
          <HolderList
            data={{ chain, holder, supply, tokenName }}
          />
        )
      default:
        return child
    }
  }

  handleSelect(e, key) {
    e.preventDefault()

    const { location, params, loadToken } = this.props
    loadToken(location.query.chain, params.token, key)
    .then(({ type, payload }) => {
      this.setState({
        activeKey: key
      })
    })
  }

  handlePagination(page) {
    const { activeKey } = this.state
    const { location, params, loadToken } = this.props
    loadToken(location.query.chain, params.token, activeKey, page)
  }
}

Token.propTypes = {
  loadToken: PropTypes.func.isRequired,
  token: PropTypes.shape({
    tx: PropTypes.array.isRequired,
    no_page: PropTypes.number.isRequired,
    pages: PropTypes.number.isRequired
  }).isRequired,
  txSwitchType: PropTypes.string.isRequired
}

export default connect(
  state => ({
    token: state.token,
    txSwitchType: state.ui.txSwitchType
  }),

  dispatch => bindActionCreators({
    loadToken
  }, dispatch)
)(Token)
