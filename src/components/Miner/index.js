import React, { PropTypes, Component } from 'react'
import bindActionCreators from 'redux/lib/bindActionCreators'
import connect from 'react-redux/lib/components/connect'
import loadMiner from '../../actions/miner'
import Pagination from 'react-bootstrap/lib/Pagination'
import Link from 'react-router/lib/Link'
import Price from '../Price'
import moment from 'moment'
import digits from '../../lib/digits'

import chainPattern from '../../constants/chainPattern'

import 'moment/locale/zh-cn'
moment.locale('zh')

if (process.env.BROWSER) {
  require('./index.scss')
}


class Miner extends Component {
  static fetchData({ store, location, params }) {
    return store.dispatch(loadMiner(location.query.chain, params.miner))
  }

  constructor() {
    super()
    this.state = {
      hasCorrectChainName: false
    }
    this.handlePagination = this.handlePagination.bind(this)
  }

  componentWillMount() {
    if (chainPattern.test(this.props.location.query.chain)) {
      this.setState({
        hasCorrectChainName: true
      })
    }
  }

  componentDidMount() {
    const { location, params, loadMiner } = this.props
    loadMiner(location.query.chain, params.miner)
  }

  render() {
    const { hasCorrectChainName } = this.state
    const {
      'location': { query: { chain } },
      miner
    } = this.props

    return hasCorrectChainName ? (
      <div className="container">
        <div className="miner">
          <ul className="breadcrumb">
            <li><Link to="/">首页</Link></li>
            <li><span>矿工</span></li>
            <li className="active"><span>{miner.relay}</span></li>
          </ul>

          <div className="page-header">
            <h1>{miner.relay}</h1>
          </div>

          <div>
            <div className="row text-center summ">
              <div className="col-sm-5">
                <span style={{ fontSize: 30 }}>
                  {digits(miner.totalcount)}
                </span>
                <br />
                <span>区块总数</span>
              </div>
              <div className="col-sm-5">
                <div>
                  <span style={{ fontSize: 30 }}>
                    <Price value={miner.receive} />
                  </span>
                  <em className="text-muted">BTC</em>
                  <br />
                  <span>挖矿总收入</span>
                </div>
              </div>
            </div>

            <div>
              <h2 className="hidden">区块列表</h2>

              {miner.blocks.length ? (
                <div>
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>区块高度</th>
                        <th>收益<sup className="text-muted">&#47;BTC</sup></th>
                        <th>哈希</th>
                        <th>主链</th>
                        <th>时间</th>
                      </tr>
                    </thead>
                    <tbody>
                      {miner.blocks.map((block, index) => (
                        <tr key={index}>
                          <td><Link to={`/block/${block.height}?chain=${chain}`}>{digits(block.height)}</Link></td>
                          <td><Price value={block.fee} /></td>
                          <td><code>{block.hash}</code></td>
                          <td>
                            {block.main === 1 ? (
                              <span className="text-success">主链</span>
                            ) : (
                              <span>非主链</span>
                            )}
                          </td>
                          <td>{moment(block.time*1000).fromNow()}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              ) : (
                <div className="alert alert-warning">No blocks.</div>
              )}

              <div className="pager-container">
                <div>总共 {miner.pages} 页 {miner.totalcount} 个区块</div>
                <Pagination
                  prev
                  next
                  maxButtons={10}
                  activePage={miner.no_page}
                  items={miner.pages}
                  onSelect={this.handlePagination}
                />
              </div>
            </div>

          </div>
        </div>
      </div>
    ) : null
  }

  handlePagination(page) {
    const { location, params, loadMiner } = this.props
    loadMiner(location.query.chain, params.miner, page)
  }
}

Miner.propTypes = {
  loadMiner: PropTypes.func.isRequired,
  miner: PropTypes.shape({
    blocks: PropTypes.array.isRequired,
    no_page: PropTypes.number.isRequired,
    pages: PropTypes.number.isRequired
  }).isRequired
}

export default connect(
  state => ({
    miner: state.miner
  }),

  dispatch => bindActionCreators({
    loadMiner
  }, dispatch)
)(Miner)
