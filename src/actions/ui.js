import actionTypes from '../constants/actionTypes'
import cookie from 'cookie'

export function toggleDisplayStyle(style) {
  document.cookie = cookie.serialize(
    'displayStyle',
    style,
    {
      path: '/',
      maxAge: 60*60*24*30
    }
  )

  return {
    type: actionTypes.ui.toggleDisplayStyle,
    payload: {
      displayStyle: style
    }
  }
}

export function toggleTxSwitcher(type) {
  return {
    type: actionTypes.ui.toggleTxSwitcher,
    payload: {
      txSwitchType: type
    }
  }
}
