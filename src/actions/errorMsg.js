import actionTypes from '../constants/actionTypes'

export function showError(error) {
  return {
    type: actionTypes.errorMsg.showError,
    payload: {
      'error': error
    }
  }
}

export function dismissError() {
  return {
    type: actionTypes.errorMsg.dismissError,
    payload: {
      'error': {
        'status': 'info',
        message: null
      }
    }
  }
}
