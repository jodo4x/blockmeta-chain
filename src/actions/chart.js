import axios from '../lib/axios'
import BASE_URL from '../constants/baseUrl'
import actionTypes from '../constants/actionTypes'
import { showError } from './errorMsg'

export default function loadChart(type, chain = 'bitcoin', chart, period) {
  return (dispatch) =>
    axios.get(`${BASE_URL}/stat/${type}?chain=${chain}&chart=${chart}&period=${period}`)
    .then((res) => {
      if (res.error) {
        return dispatch(showError(res.error))
      }

      return dispatch({
        type: actionTypes.chart.loadChart,
        payload: {
          chart: res.data.data
        }
      })
    })
}
