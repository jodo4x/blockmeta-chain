import axios from '../lib/axios'
import BASE_URL from '../constants/baseUrl'
import actionTypes from '../constants/actionTypes'
import { showError } from './errorMsg'

export default function loadArchives(chain = 'bitcoin', page = 1, tag = 'all') {
  return (dispatch) =>
    axios.get(`${BASE_URL}/archives?chain=${chain}&page=${page}&tag=${tag}`)
    .then((res) => {
      if (res.error) {
        return dispatch(showError(res.error))
      }

      return dispatch({
        type: actionTypes.archives.loadArchives,
        payload: {
          ...res.data.data
        }
      })
    })
}
