import axios from '../lib/axios'
import BASE_URL from '../constants/baseUrl'
import actionTypes from '../constants/actionTypes'
import { showError } from './errorMsg'

export default function loadAddress(
  chain = 'bitcoin',
  id,
  page = 1,
  filter = 'all' // NOTE: 只有Bitcoin的地址才有
) {
  return (dispatch) =>
    axios.get(`${BASE_URL}/address/${id}?chain=${chain}&page=${page}&filter=${filter}`)
    .then((res) => {
      if (res.error) {
        return dispatch(showError(res.error))
      }

      return dispatch({
        type: actionTypes.address.loadAddress,
        payload: {
          ...res.data.data
        }
      })
    })
}
