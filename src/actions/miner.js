import axios from '../lib/axios'
import BASE_URL from '../constants/baseUrl'
import actionTypes from '../constants/actionTypes'
import { showError } from './errorMsg'

export default function loadMiner(chain = 'bitcoin', miner, page = 1) {
  return (dispatch) =>
    axios.get(`${BASE_URL}/miner/${miner}?chain=${chain}&page=${page}`)
    .then((res) => {
      if (res.error) {
        return dispatch(showError(res.error))
      }

      return dispatch({
        type: actionTypes.miner.loadMiner,
        payload: {
          ...res.data.data
        }
      })
    })
}
