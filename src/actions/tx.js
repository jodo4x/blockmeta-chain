import axios from '../lib/axios'
import BASE_URL from '../constants/baseUrl'
import actionTypes from '../constants/actionTypes'
import { showError } from './errorMsg'

export default function loadTx(chain = 'bitcoin', id) {
  return (dispatch) =>
    axios.get(`${BASE_URL}/tx/${id}?chain=${chain}&detail=1`)
    .then((res) => {
      if (res.error) {
        return dispatch(showError(res.error))
      }

      return dispatch({
        type: actionTypes.tx.loadTx,
        payload: {
          ...res.data.data
        }
      })
    })
}
