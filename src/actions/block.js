import axios from '../lib/axios'
import BASE_URL from '../constants/baseUrl'
import actionTypes from '../constants/actionTypes'
import { showError } from './errorMsg'

export default function loadBlock(chain = 'bitcoin', id, page = 1) {
  return (dispatch) =>
    axios.get(`${BASE_URL}/block/${id}?chain=${chain}&page=${page}`)
    .then((res) => {
      if (res.error) {
        return dispatch(showError(res.error))
      }

      return dispatch({
        type: actionTypes.block.loadBlock,
        payload: {
          // `txs`, `no_page`, `pages`
          ...res.data.data
        }
      })
    })
}
