import axios from '../lib/axios'
import BASE_URL from '../constants/baseUrl'
import actionTypes from '../constants/actionTypes'
import { showError } from './errorMsg'

export default function loadToken(chain = 'ethereum', token, filter = 'tx', page = 1) {
  return (dispatch) =>
    axios.get(`${BASE_URL}/token/${token}?chain=${chain}&filter=${filter}&page=${page}`)
    .then((res) => {
      if (res.error) {
        return dispatch(showError(res.error))
      }

      return dispatch({
        type: actionTypes.token.loadToken,
        payload: {
          ...res.data.data
        }
      })
    })
}
