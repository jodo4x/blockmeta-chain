import axios from '../lib/axios'
import { push } from 'react-router-redux/lib/actions'
import assign from '../lib/assign'
import BASE_URL from '../constants/baseUrl'
import actionTypes from '../constants/actionTypes'
import { showError } from './errorMsg'

const defaultChain = 'bitcoin'

function loadSearchResult(chain, type, value, detail = 0) {
  return (dispatch) =>
    axios.get(`${BASE_URL}/${type}/${value}?chain=${chain}&detail=${detail}`)
    .then((res) => {

      // FIXME: failure ?
      if (res.error) {
        return dispatch(showError(res.error))
      }

      return dispatch(push(`/${type}/${value}?chain=${chain}`))
    })
}

export function loadSearch(chain = defaultChain, value = '') {
  console.warn('================')
  return (dispatch) => {
    console.info('----------------')
    return axios.post(`${BASE_URL}/search`, {
      chain: chain,
      q: value
    })
    .then((res) => {
      if (res.error) {
        return dispatch(showError({
          'status': 'danger',
          message: '找不到匹配的项目，请再试试。'
        }))
      }

      const { chain, type, value } = res.data.data
      return dispatch(loadSearchResult(chain, type, value))
    })
  }
}

export function loadBlocks(chain = defaultChain, page = 1) {
  return (dispatch) =>
    axios.get(`${BASE_URL}/blocks?chain=${chain}&page=${page}`)
    .then((res) => {
        if (res.error) {
          return dispatch(showError(res.error))
        }

        return dispatch({
          type: actionTypes.homepage.loadBlocks,
          payload: {
            items: res.data.data.blocks, // NOTE: `blocks` --> `items`
            no_page: res.data.data.no_page,
            pages: res.data.data.pages
          }
        })
    })
}
