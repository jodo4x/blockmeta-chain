import axios from '../lib/axios'
import BASE_URL from '../constants/baseUrl'
import actionTypes from '../constants/actionTypes'
import { showError } from './errorMsg'

export default function loadStats(chain = 'bitcoin') {
  return (dispatch) =>
    axios.get(`${BASE_URL}/stat/blockchain?chain=${chain}`)
    .then((res) => {
      if (res.error) {
        return dispatch(showError(res.error))
      }

      return dispatch({
        type: actionTypes.stats.loadStats,
        payload: {
          ...res.data.data
        }
      })
    })
}
