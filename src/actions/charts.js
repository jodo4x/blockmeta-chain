import axios from '../lib/axios'
import BASE_URL from '../constants/baseUrl'
import actionTypes from '../constants/actionTypes'
import { showError } from './errorMsg'

export const charts = [
  'block:total_fees',
  'block:block_avg_size',
  'block:block_avg_interval',
  'chain:totalbtc',
  'chain:blockchain_size',
  'chain:hash_rate'
]

export const pattern = /(\w+):(\w+)/

export function parseParams(params) {
  return charts
    .find(value => value.indexOf(params.chart) !== -1)
    .match(pattern)
}

// TODO: phantomjs export PNG
export default function loadCharts(chain = 'bitcoin', period = 365) {
  return (dispatch) =>
    Promise.all(
      charts.map((chart) => {
        const match = chart.match(pattern)
        return axios.get(
          `${BASE_URL}/stat/${match[1]}?chain=${chain}&chart=${match[2]}&period=${period}`
        )
      })
    )
    .then(
      res => dispatch({
        type: actionTypes.charts.loadCharts,
        payload: {
          charts: res.map(r => r.data.data)
        }
      })
    )
}
