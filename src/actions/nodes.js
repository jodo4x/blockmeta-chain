import axios from '../lib/axios'
import BASE_URL from '../constants/baseUrl'
import actionTypes from '../constants/actionTypes'
import { showError } from './errorMsg'

const defaultChain = 'bitcoin'

export function loadDist(chain = defaultChain) {
  return (dispatch) =>
    axios.get(`${BASE_URL}/nodedist?chain=${chain}`)
    .then((res) => {
      if (res.error) {
        return dispatch(showError(res.error))
      }

      return dispatch({
        type: actionTypes.nodes.loadDist,
        payload: {
          dist: res.data.data
        }
      })
    })
}

export function loadNodes(chain = defaultChain, page = 1) {
  return (dispatch) =>
    axios.get(`${BASE_URL}/nodes?chain=${chain}&page=${page}&per_page=20`)
    .then((res) => {
      if (res.error) {
        return dispatch(showError(res.error))
      }

      return dispatch({
        type: actionTypes.nodes.loadNodes,
        payload: {
          nodes: res.data.data
        }
      })
    })
}

export function searchNodes(chain = defaultChain, ip = '') {
  return (dispatch) =>
    axios.get(`${BASE_URL}/nodes?chain=${chain}&ip=${ip}`)
    .then((res) => {
      if (res.error) {
        return dispatch(showError(res.error))
      }

      return dispatch({
        type: actionTypes.nodes.searchNodes,
        payload: {
          nodes: res.data.data
        }
      })
    })
}
