import assign from '../lib/assign'
import actionTypes from '../constants/actionTypes'

const initialState = {
  txs: [],
  tx: [], // 以太坊交易
  token_flags: [] // 以太坊
}

export default function address(state = initialState, { type, payload }) {
  if (type === actionTypes.address.loadAddress) {
    return assign({}, state, payload)
  }

  return state
}
