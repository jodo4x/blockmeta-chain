import combineReducers from 'redux/lib/combineReducers'
import { routerReducer as routing } from 'react-router-redux/lib/reducer'

import errorMsg from './errorMsg'
import ui from './ui'
import homepage from './homepage'
import block from './block'
import tx from './tx'
import address from './address'
import miner from './miner'
import token from './token'
import stats from './stats'
import charts from './charts'
import chart from './chart'
import archives from './archives'
import nodes from './nodes'

export default combineReducers({
  routing,
  errorMsg,
  ui,
  homepage,
  block,
  tx,
  address,
  miner,
  token,
  stats,
  charts,
  chart,
  archives,
  nodes
})
