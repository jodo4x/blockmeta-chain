import actionTypes from '../constants/actionTypes'
import assign from '../lib/assign'

const initialState = {
  charts: []
}

export default function charts(state = initialState, { type, payload }) {
  switch(type) {
    case actionTypes.charts.loadCharts:
      return assign({}, state, payload)
    default:
      return state
  }
}
