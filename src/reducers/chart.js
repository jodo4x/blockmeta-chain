import assign from '../lib/assign'
import actionTypes from '../constants/actionTypes'

const initialState = {
  chart: []
}

export default function chart(state = initialState, { type, payload }) {
  switch(type) {
    case actionTypes.chart.loadChart:
      return assign({}, state, payload)
    default:
      return state
  }
}
