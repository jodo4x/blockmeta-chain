import assign from '../lib/assign'
import actionTypes from '../constants/actionTypes'

const initialState = {
  tx: [],
  no_page: 1,
  pages: 1,
  transactions: [] // NOTE: 这是ethereum的交易名
}

export default function block(state = initialState, { type, payload }) {
  switch(type) {
    case actionTypes.block.loadBlock:
      return assign({}, state, payload)
    default:
      return state
  }
}
