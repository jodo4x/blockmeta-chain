import assign from '../lib/assign'
import actionTypes from '../constants/actionTypes'

const initialState = {
  'error': {
    'status': 'info',
    message: null
  }
}

export default function errorMsg(state = initialState, { type, payload }) {
  switch(type) {
    case actionTypes.errorMsg.showError:
    case actionTypes.errorMsg.dismissError:
      return assign({}, state, payload)
    default:
      return state
  }
}
