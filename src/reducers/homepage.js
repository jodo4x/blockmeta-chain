import actionTypes from '../constants/actionTypes'
import assign from '../lib/assign'

const initialState = {
  items: [],
  no_page: 1,
  pages: 1
}

export default function homepage(state = initialState, { type, payload }) {
  switch(type) {
    case actionTypes.homepage.loadBlocks:
      return assign({}, state, payload)
    default:
      return state
  }
}
