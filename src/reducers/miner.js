import assign from '../lib/assign'
import actionTypes from '../constants/actionTypes'

const initialState = {
  blocks: [],
  no_page: 1,
  pages: 1
}

export default function miner(state = initialState, { type, payload }) {
  switch(type) {
    case actionTypes.miner.loadMiner:
      return assign({}, state, payload)
    default:
      return state
  }
}
