import actionTypes from '../constants/actionTypes'
import assign from '../lib/assign'

const initialState = {
  dist: {},
  nodes: {}
}

export default function nodes(state = initialState, { type, payload }) {
  switch(type) {
    case actionTypes.nodes.loadDist:
    case actionTypes.nodes.loadNodes:
    case actionTypes.nodes.searchNodes:
      return assign({}, state, payload)
    default:
      return state
  }
}
