import assign from '../lib/assign'
import actionTypes from '../constants/actionTypes'

const initialState = {
  stat: {},
  archives: [],
  no_page: 1,
  pages: 1
}

export default function arvhices(state = initialState, { type, payload }) {
  if (type === actionTypes.archives.loadArchives) {
    return assign({}, state, payload)
  }

  return state
}
