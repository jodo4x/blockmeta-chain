import actionTypes from '../constants/actionTypes'
import assign from '../lib/assign'

const initialState = {
  pool_dist: {},
  blockchain: {},
  market: {}
}

export default function charts(state = initialState, { type, payload }) {
  switch(type) {
    case actionTypes.stats.loadStats:
      return assign({}, state, payload)
    default:
      return state
  }
}
