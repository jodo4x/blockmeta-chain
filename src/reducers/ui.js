import assign from '../lib/assign'
import actionTypes from '../constants/actionTypes'

const initialState = {
  displayStyle: 'normal',
  txSwitchType: 'h'
}

export default function ui(state = initialState, { type, payload }) {
  switch(type) {
    case actionTypes.ui.toggleDisplayStyle:
    case actionTypes.ui.toggleTxSwitcher:
      return assign({}, state, payload)
    default:
      return state
  }
}
