import assign from '../lib/assign'
import actionTypes from '../constants/actionTypes'

const initialState = {
  holder: [],
  tx: [],
  no_page: 1,
  pages: 1
}

export default function token(state = initialState, { type, payload }) {
  switch(type) {
    case actionTypes.token.loadToken:
      return assign({}, state, payload)
    default:
      return state
  }
}
