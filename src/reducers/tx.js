import assign from '../lib/assign'
import actionTypes from '../constants/actionTypes'

const initialState = {
  inputs: [],    // 比特币输入地址
  outputs: [],   // 比特币输出地址
  token_txs: [], // 以太坊
  tx: {          // 以太坊
    logs: []
  }
}

export default function tx(state = initialState, { type, payload }) {
  switch(type) {
    case actionTypes.tx.loadTx:
      return assign({}, state, payload)
    default:
      return state
  }
}
