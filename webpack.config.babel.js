import webpack from 'webpack'
import ExtractTextPlugin from 'extract-text-webpack-plugin'

const plugins = [
  new webpack.DefinePlugin({
    'process.env': {
      BROWSER: JSON.stringify(true)
    }
  }),
  new ExtractTextPlugin('main.css', { allChunks: true }),
  new webpack.optimize.OccurenceOrderPlugin(),
  new webpack.optimize.DedupePlugin()
]

if (process.env.NODE_ENV === 'production') {
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compressor: { warnings: false }
    })
  )
}

export default {
  context: `${__dirname}/src`,
  entry: './client.js',
  output: {
    path: `${__dirname}/build`,
    filename: '[name].js',
    chunkFilename: '[id].chunk-[hash].js',
    publicPath: '/build/', // NOTE: last `/` is required
  },
  module: {
    loaders: [
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel' },
      { test: /\.scss$/, loader: ExtractTextPlugin.extract('css!sass') }
    ]
  },
  plugins: plugins,
  sassLoader: {
    includePaths: require('node-bourbon').includePaths
  }
}
